set(CMAKE_SYSTEM_NAME Windows)
set(CMAKE_SYSTEM_PROCESSOR x86_64)

set(TARGET_TRIPLET x86_64-w64-mingw32)

set(CMAKE_C_COMPILER x86_64-w64-mingw32-gcc-posix)
set(CMAKE_C_COMPILER_TARGET ${TARGET_TRIPLET})
set(CMAKE_CXX_COMPILER x86_64-w64-mingw32-g++-posix)
set(CMAKE_CXX_COMPILER_TARGET ${TARGET_TRIPLET})

file(MAKE_DIRECTORY ../build)
file(MAKE_DIRECTORY ../build/${TARGET_TRIPLET})