#!/usr/bin/env bash

if [[ -z ${WINEPREFIX} ]]; then
    WINEPREFIX="${HOME}/.wine"
fi

cp /usr/lib/gcc/x86_64-w64-mingw32/8.3-win32/libgcc_s_seh-1.dll ${WINEPREFIX}/drive_c/windows/system32/
cp /usr/lib/gcc/x86_64-w64-mingw32/8.3-win32/libstdc++-6.dll ${WINEPREFIX}/drive_c/windows/system32/
cp /usr/x86_64-w64-mingw32/lib/libwinpthread-1.dll ${WINEPREFIX}/drive_c/windows/system32/
