#!/usr/bin/env bash

mkdir -p ../packages

triplet="clang-x86_64-pc-linux-gnu"
file="${PWD}/builds/${triplet}"

date=$(date +%Y-%m-%d_%H:%M:%S)

if [[ -z ${CI_COMMIT_BEFORE_SHA} ]]; then
    CI_COMMIT_BEFORE_SHA=0
fi

file_format='ra-engine_'${CI_COMMIT_BEFORE_SHA}'_'${triplet}'_'${date}

cd ..

project_dir=${PWD}

mkdir -p ${project_dir}/packages
mkdir -p ${project_dir}/packages/${triplet}

cp    ${project_dir}/builds/${triplet}/source/ra-engine-test/ra-engine-test ${project_dir}/packages/${triplet}
cp    ${project_dir}/builds/${triplet}/ext/Box2D/Box2D/libBox2D.so ${project_dir}/packages/${triplet}
cp -r ${project_dir}/assets ${project_dir}/packages/${triplet}
tar -C ${project_dir}/packages/ -cvzf ${project_dir}/packages/${file_format}.tar.gz ${triplet}