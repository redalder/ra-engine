#!/usr/bin/env bash

cd ../ext/llvm
git checkout stable
cd tools
git clone https://git.llvm.org/git/clang
cd clang-    
git checkout release_90
cd ../
git clone https://github.com/vgvassilev/cling
