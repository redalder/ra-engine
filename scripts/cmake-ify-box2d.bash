#!/usr/bin/env bash

cd ../ext/Box2D/
cp $PWD/../Box2D-cmake/Box2D/CMakeLists.txt CMakeLists.txt
cp $PWD/../Box2D-cmake/Box2D/Box2D/CMakeLists.txt Box2D/CMakeLists.txt
cp $PWD/../Box2D-cmake/Box2D/Box2D/FindBox2D.cmake Box2D/FindBox2D.cmake