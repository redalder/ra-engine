#!/usr/bin/env bash

for file in ../scripts/build-clang-*.bash; do
    [[ -e "${file}" ]] || continue

    bash "${file}" $1
done