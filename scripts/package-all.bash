#!/usr/bin/env bash

for file in ../scripts/package-clang-*.bash; do
    [[ -e "${file}" ]] || continue

    bash "${file}"
done