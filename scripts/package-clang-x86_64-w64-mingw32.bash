#!/usr/bin/env bash

mkdir -p ../packages

triplet="clang-x86_64-w64-mingw32"
file="${PWD}/builds/${triplet}"

date=$(date +%Y-%m-%d_%H:%M:%S)

if [[ -z ${CI_COMMIT_BEFORE_SHA} ]]; then
    CI_COMMIT_BEFORE_SHA=0
fi

file_format='ra-engine_'${CI_COMMIT_BEFORE_SHA}'_'${triplet}'_'${date}

cd ..

project_dir=${PWD}

mkdir -p ${project_dir}/packages
mkdir -p ${project_dir}/packages/${triplet}

cp ${project_dir}/builds/${triplet}/source/ra-engine-test/ra-engine-test.exe ${project_dir}/packages/${triplet}
cp /usr/lib/gcc/x86_64-w64-mingw32/8.3-win32/libgcc_s_seh-1.dll ${project_dir}/packages/${triplet}
cp /usr/lib/gcc/x86_64-w64-mingw32/8.3-win32/libstdc++-6.dll ${project_dir}/packages/${triplet}
cp /usr/x86_64-w64-mingw32/lib/libwinpthread-1.dll ${project_dir}/packages/${triplet}
cp ${project_dir}/builds/${triplet}/ext/Box2D/Box2D/libBox2D.dll ${project_dir}/packages/${triplet}
cp -r ${project_dir}/assets ${project_dir}/packages/${triplet}
tar -C ${project_dir}/packages/ -cvzf ${project_dir}/packages/${file_format}.tar.gz ${triplet}