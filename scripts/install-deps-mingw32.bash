#!/usr/bin/env bash

sudo rsync -av ${PWD}/../ext/mingw-w64-glfw/ /usr/x86_64-w64-mingw32
sudo rsync -av ${PWD}/../ext/mingw-w64-vulkan-headers/ /usr/x86_64-w64-mingw32
sudo rsync -av ${PWD}/../ext/mingw-w64-vulkan-icd-loader/ /usr/x86_64-w64-mingw32
#sudo rsync -av ${PWD}/../ext/mingw-w64-winpthreads/ /usr/x86_64-w64-mingw32