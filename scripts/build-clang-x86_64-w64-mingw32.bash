#!/usr/bin/env bash

triplet=clang-x86_64-w64-mingw32

echo $1
build_type=$1

if [[ "$build_type" != "Release" ]] && [[ "$build_type" != "Debug" ]]; then
	build_type="Debug"
fi

mkdir -p ../builds
mkdir -p ../builds/${triplet}
cd ../builds/${triplet}
cmake ../.. -DCMAKE_TOOLCHAIN_FILE=../../toolchains/${triplet}.cmake -DCMAKE_BUILD_TYPE=${build_type}
make -j 12
