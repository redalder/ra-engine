//
// Created by main on 5/10/19.
//

#include <memory>
#include <fstream>

#include <spdlog/spdlog.h>

#include <stb_image.h>
#include <stb_image_write.h>
#include <ra/math/vector2.hpp>

#include "ra/renderer/texture_atlas_raw.hpp"

ra::renderer::texture_atlas_raw::texture_atlas_raw(const ra::math::vector2u &p_size) {
  size = p_size;
  compression_ration = ra::math::vector2u(1, 1);
  type = raw;

  m_data.resize(size.x*size.y, pixel_rgba(0, 0, 0, 0));

  m_textures.emplace_back(std::make_shared<texture>(ra::math::aabb2u(0, 0, size - 1), true));
}

/// ra::renderer:texture_atlas::allocate returns a texture, if it can allocate it based on the given size.
/// @param p_size 2D vector which specifies the size of the allocation.
/// @return Texture, if allocation is successful.
std::optional<std::weak_ptr<ra::renderer::texture>>
ra::renderer::texture_atlas_raw::allocate(const ra::math::vector2u &p_size) {
  std::shared_ptr<texture> target;

  for (auto &texture : m_textures) {
    auto size = texture->aabb.size();

    if (texture->aabb.size()==p_size && texture->free) {
      texture->free = false;
      return texture;
    } else if (texture->aabb.size() >= p_size && texture->free) {
      target = texture;
      break;
    }
  }

  if (!target) {
    return std::nullopt;
  }

  auto parent_aabb = target->aabb;
  target->aabb = ra::math::aabb2u(parent_aabb.a, parent_aabb.a + p_size - ra::math::vector2u(1, 1));
  target->free = false;

  m_textures.emplace_back(std::make_shared<texture>(ra::math::aabb2u(
      ra::math::vector2u(target->aabb.b.x + 1, target->aabb.a.y),
      parent_aabb.b),
                                                    true));

  if (m_textures.at(m_textures.size() - 1)->aabb.size()==0)
    m_textures.erase(m_textures.end());

  m_textures.emplace_back(std::make_shared<texture>(ra::math::aabb2u(
      ra::math::vector2u(target->aabb.a.x, target->aabb.b.y + 1),
      ra::math::vector2u(target->aabb.b.x, parent_aabb.b.y)),
                                                    true));

  if (m_textures.at(m_textures.size() - 1)->aabb.size()==0)
    m_textures.erase(m_textures.end());

  m_dirty_percent_free = true;

  return target;
}

/// ra::renderer::texture_atlas::deallocate
/// @param p_texture Texture to be deallocated;
/// @return Returns true, if the texture belongs to this texture atlas, false otherwise.
bool ra::renderer::texture_atlas_raw::deallocate(const std::weak_ptr<ra::renderer::texture> &p_texture) {
  bool found = false;

  for (auto &texture : m_textures) {
    if (texture==p_texture.lock()) {
      found = true;
      break;
    }
  }

  if (!found)
    return false;

  p_texture.lock()->free = true;

  return true;
}

bool ra::renderer::texture_atlas_raw::optimize() {
  // @TODO implement a optimization algorithm (merge adjacent free texture into bigger textures)

  m_dirty_percent_free = true;

  return false;
}

std::pair<bool, bool>
ra::renderer::texture_atlas_raw::deallocate_and_optimize(const std::weak_ptr<ra::renderer::texture> &p_texture) {
  return std::pair<bool, bool>(deallocate(p_texture), optimize());
}

std::optional<std::weak_ptr<ra::renderer::texture>> ra::renderer::texture_atlas_raw::load(const std::string &path) {
  int x;
  int y;
  int channels;

  unsigned char *data = stbi_load(path.c_str(), &x, &y, &channels, STBI_rgb_alpha);

  if (!data) {
    spdlog::default_logger()->info("File not found: \"{}\"", path);
    return std::nullopt;
  }

  switch (channels) {
  case 1:
  case 2:spdlog::default_logger()->info("Unsupported amount of channels ({}) in image file: \"{}\"", channels, path);
    return std::nullopt;
  case 3:
  case 4: {
    auto opt_texture = allocate(ra::math::vector2u(x, y));
    if (opt_texture.has_value()) {
      auto texture = opt_texture.value().lock();
      const auto &aabb = texture->aabb;

      long offset = 0;

      if (aabb.a.x==0)
        offset = aabb.a.y*size.x;
      else if (aabb.a.y==0)
        offset = aabb.a.x;
      else
        offset = aabb.a.y*size.x + aabb.a.x;

      for (int i = 0; i < y; i++) {
        void *dest = m_data.data() + offset + i*size.x;
        void *src = data + i*x*4;

        memcpy(dest, src, x*4);
      }

      stbi_image_free(data);

      return texture;
    } else {

      stbi_image_free(data);

      return std::nullopt;
    }
  }
  default:stbi_image_free(data);

    return std::nullopt;
  }
}

bool ra::renderer::texture_atlas_raw::save_png(const std::string &path) {
  int result = stbi_write_png(path.c_str(), size.x, size.y, 4, m_data.data(), 0);

  return result;
}

void ra::renderer::texture_atlas_raw::debug() {
  spdlog::default_logger()->info("Debug started for texture atlas: {}", reinterpret_cast<std::intptr_t>(this));

  for (auto &texture : m_textures) {
    const auto &aabb = texture->aabb;
    spdlog::default_logger()->info("\t[{}, {}, {}, {}] is {}", aabb.a.x, aabb.a.y, aabb.b.x, aabb.b.y,
                                   texture->free ? "free" : "used");
  }
}

unsigned short ra::renderer::texture_atlas_raw::percent_free() {
  if (!m_dirty_percent_free)
    return m_cached_percent_free;

  long area_free = 0;

  for (auto &texture : m_textures) {
    if (texture->free)
      area_free += texture->aabb.area();
  }

  m_cached_percent_free = static_cast<unsigned short>(area_free/static_cast<float>(size.x*size.y)*100.0f);
  m_dirty_percent_free = false;

  return m_cached_percent_free;
}

void *ra::renderer::texture_atlas_raw::data() {
  return m_data.data();
}