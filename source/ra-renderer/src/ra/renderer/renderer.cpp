/*
 * This file was created by Magic_RB on 2019-05-19
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#include <ra/engine/component/sprite.hpp>
#include <ra/engine/component/transform.hpp>

#include "ra/renderer/renderer.hpp"

ra::renderer::renderer::renderer(const ra::renderer::renderer_create_info &p_create_info) {
  if (!p_create_info.registry)
    return;

  m_registry = p_create_info.registry;
  window_size = p_create_info.window_size;
  m_z_near = p_create_info.z_near;
  m_z_far = p_create_info.z_far;
  m_visible_tiles = p_create_info.visible_tiles;

  m_vertex_shader = ra::gl::shader(GL_VERTEX_SHADER, m_vertex_shader_str);
  m_fragment_shader = ra::gl::shader(GL_FRAGMENT_SHADER, m_fragment_shader_str);

  m_program = std::make_shared<ra::gl::program>(m_vertex_shader, m_fragment_shader);
  m_program->use();

  m_projection_location = m_program->get_location("projection_matrix");
  m_translation_location = m_program->get_location("translation_matrix");
  m_texture_location = m_program->get_location("in_texture");

  m_update_projection(window_size.y/window_size.x);
  m_translation = ra::math::matrix_4x4_f::translation(ra::math::vector3f(1, 0, 0));
}

void ra::renderer::renderer::window_resized(int p_width, int p_height) {
  window_size = ra::math::vector2f(p_width, p_height);
  m_update_projection(static_cast<float>(p_height)/static_cast<float>(p_width));

  for (auto &pair : m_atlas_map) {
    auto &render_batch = pair.second;

    render_batch->vao.bind();
    glUniformMatrix4fv(m_projection_location, 1, GL_TRUE, &m_projection.data[0]);
  }

  glViewport(0, 0, p_width, p_height);
}

void ra::renderer::renderer::build() {
  auto view = m_registry->view<ra::engine::component::sprite, ra::engine::component::transform>();

  for (auto &pair : m_atlas_map) {
    auto &render_batch = pair.second;

    render_batch->vertices.clear();
  }

  for (auto &entity : view) {
    auto &transform = m_registry->get<ra::engine::component::transform>(entity);
    auto &sprite = m_registry->get<ra::engine::component::sprite>(entity);

    if (m_atlas_map.count(std::shared_ptr(sprite.texture_atlas)) < 1) {
      m_create_renderer_batch(sprite.texture_atlas.lock());
    }

    auto &render_batch = m_atlas_map.at(std::shared_ptr(sprite.texture_atlas));

    render_batch->vertices.emplace_back(
        ra::math::vector3f(-0.5f*transform.size.x, -0.5f*transform.size.y, 0.0f) + transform.position,
        sprite.uv.a);
    render_batch->vertices.emplace_back(
        ra::math::vector3f(0.5f*transform.size.x, -0.5f*transform.size.y, 0.0f) + transform.position,
        sprite.uv.c());
    render_batch->vertices.emplace_back(
        ra::math::vector3f(0.5f*transform.size.x, 0.5f*transform.size.y, 0.0f) + transform.position,
        sprite.uv.b);
    render_batch->vertices.emplace_back(
        ra::math::vector3f(-0.5f*transform.size.x, -0.5f*transform.size.y, 0.0f) + transform.position,
        sprite.uv.a);
    render_batch->vertices.emplace_back(
        ra::math::vector3f(0.5f*transform.size.x, 0.5f*transform.size.y, 0.0f) + transform.position,
        sprite.uv.b);
    render_batch->vertices.emplace_back(
        ra::math::vector3f(-0.5f*transform.size.x, 0.5f*transform.size.y, 0.0f) + transform.position,
        sprite.uv.d());
  }

  for (auto &pair : m_atlas_map) {
    auto &render_batch = pair.second;

    render_batch->vao.bind();
    render_batch->vbo.set_data(render_batch->vertices.data(), render_batch->vertices.size()*4*5);
  }
}

void ra::renderer::renderer::render() {
  for (auto &pair : m_atlas_map) {
    auto &render_batch = pair.second;

    glActiveTexture(GL_TEXTURE0);
    render_batch->texture_2d.bind();

    m_program->use();
    render_batch->vao.bind();

    glDrawArrays(GL_TRIANGLES, 0, render_batch->vertices.size());
  }
}

void
ra::renderer::renderer::m_create_renderer_batch(const std::shared_ptr<ra::renderer::texture_atlas> &p_texture_atlas) {
  m_atlas_map.insert(
      std::pair<std::shared_ptr<ra::renderer::texture_atlas>, std::shared_ptr<render_batch>>(p_texture_atlas,
                                                                                             std::make_shared<
                                                                                                 render_batch>()));
  auto &render_batch = m_atlas_map.at(p_texture_atlas);

  render_batch->vao.gen();
  render_batch->vao.bind();
  render_batch->vbo.gen();
  render_batch->vbo.bind();
  render_batch->texture_2d.gen();
  render_batch->texture_2d.set_data(p_texture_atlas->data(), p_texture_atlas->size);
  render_batch->texture_atlas = p_texture_atlas;

  glActiveTexture(GL_TEXTURE0);
  glUniform1i(m_texture_location, 0);
  glUniformMatrix4fv(m_projection_location, 1, GL_TRUE, &m_projection.data[0]);
  glUniformMatrix4fv(m_translation_location, 1, GL_TRUE, &m_translation.data[0]);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void *) offsetof(vertex, position));
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (void *) offsetof(vertex, uv));
}

void ra::renderer::renderer::m_update_projection(float aspect_ratio) {
  if (aspect_ratio > 1.0f) {
    float left = m_visible_tiles.x/-2 + 1.0f;
    float right = m_visible_tiles.x/2 + 1.0f;
    float bottom = aspect_ratio*m_visible_tiles.x/-2;
    float top = aspect_ratio*m_visible_tiles.x/2;
    m_projection = ra::math::matrix_4x4_f::orthographic_projection(left, right, bottom, top, m_z_near, m_z_far);
  } else {
    float left = 1.0f/aspect_ratio*m_visible_tiles.y/-2 + 1.0f;
    float right = 1.0f/aspect_ratio*m_visible_tiles.y/2 + 1.0f;
    float bottom = m_visible_tiles.y/-2;
    float top = m_visible_tiles.y/2;
    m_projection = ra::math::matrix_4x4_f::orthographic_projection(left, right, bottom, top, m_z_near, m_z_far);
  }
}
