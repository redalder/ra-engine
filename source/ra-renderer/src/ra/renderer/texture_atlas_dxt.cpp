//
// Created by main on 5/17/19.
//

#include <stb_image.h>
#include <stb_dxt.h>
#include <spdlog/spdlog.h>

#include "ra/renderer/texture_atlas_dxt.hpp"
#include "ra/renderer/texture.hpp"

ra::renderer::texture_atlas_dxt::texture_atlas_dxt(const ra::math::vector2u &p_size) {
  ra::math::vector2u adjusted_size = m_pad(p_size, 4);
  ra::math::vector2u dxt_blocks = adjusted_size/4;
  type = dxt5_compressed;

  size = dxt_blocks;
  compression_ration = ra::math::vector2u(4, 4);
  dxt_size = dxt_blocks;

  m_data.resize(dxt_blocks.x*dxt_blocks.y);
  std::fill(m_data.begin(), m_data.end(), ra::renderer::dxt_block({0, 0, 0, 0}));

  m_textures.emplace_back(std::make_shared<texture>(ra::math::aabb2u(0, 0, size*compression_ration - 1), true));
}

std::optional<std::weak_ptr<ra::renderer::texture>>
ra::renderer::texture_atlas_dxt::allocate(const ra::math::vector2u &p_size) {
  std::shared_ptr<texture> target;

  for (auto &texture : m_textures) {
    auto size = texture->aabb.size();

    if (texture->aabb.size()==p_size && texture->free) {
      texture->free = false;
      return texture;
    } else if (texture->aabb.size() >= p_size && texture->free) {
      target = texture;
      break;
    }
  }

  if (!target) {
    return std::nullopt;
  }

  auto parent_aabb = target->aabb;
  target->aabb = ra::math::aabb2u(parent_aabb.a, parent_aabb.a + p_size - ra::math::vector2u(1, 1));
  target->free = false;

  m_textures.emplace_back(std::make_shared<texture>(ra::math::aabb2u(
      ra::math::vector2u(target->aabb.b.x + 1, target->aabb.a.y),
      parent_aabb.b),
                                                    true));

  if (m_textures.at(m_textures.size() - 1)->aabb.size()==0)
    m_textures.erase(m_textures.end());

  m_textures.emplace_back(std::make_shared<texture>(ra::math::aabb2u(
      ra::math::vector2u(target->aabb.a.x, target->aabb.b.y + 1),
      ra::math::vector2u(target->aabb.b.x, parent_aabb.b.y)),
                                                    true));

  if (m_textures.at(m_textures.size() - 1)->aabb.size()==0)
    m_textures.erase(m_textures.end());

  m_dirty_percent_free = true;

  return target;
}

/// ra::renderer::texture_atlas_dxt::deallocate
/// @param p_texture Texture to be deallocated;
/// @return Returns true, if the texture belongs to this texture atlas, false otherwise.
bool ra::renderer::texture_atlas_dxt::deallocate(const std::weak_ptr<ra::renderer::texture> &p_texture) {
  bool found = false;

  for (auto &texture : m_textures) {
    if (texture==p_texture.lock()) {
      found = true;
      break;
    }
  }

  if (!found)
    return false;

  p_texture.lock()->free = true;

  return true;
}

bool ra::renderer::texture_atlas_dxt::optimize() {
  // @TODO implement a optimization algorithm (merge adjacent free texture into bigger textures)

  m_dirty_percent_free = true;

  return false;
}

std::pair<bool, bool>
ra::renderer::texture_atlas_dxt::deallocate_and_optimize(const std::weak_ptr<ra::renderer::texture> &p_texture) {
  return std::pair<bool, bool>(deallocate(p_texture), optimize());
}

std::optional<std::weak_ptr<ra::renderer::texture>> ra::renderer::texture_atlas_dxt::load(const std::string &path) {
  int x;
  int y;
  int channels;

  unsigned char *data = stbi_load(path.c_str(), &x, &y, &channels, STBI_rgb_alpha);

  if (!data) {
    spdlog::default_logger()->info("File not found: \"{}\"", path);
    return std::nullopt;
  }

  switch (channels) {
  case 1:
  case 2:spdlog::default_logger()->info("Unsupported amount of channels ({}) in image file: \"{}\"", channels, path);
    return std::nullopt;
  case 3:
  case 4: {
    auto opt_texture = allocate(m_pad(ra::math::vector2u(x, y), 4));
    if (opt_texture.has_value()) {
      auto texture = opt_texture.value().lock();
      const auto &aabb_dxt = texture->aabb/4;
      const auto &aabb = texture->aabb;

      long offset_dxt = 0;
      long offset = 0;

      if (aabb_dxt.a.x==0) {
        offset_dxt = aabb_dxt.a.y*size.x;
        offset = aabb.a.y*size.x;
      } else {
        offset_dxt = aabb_dxt.a.y*size.x + aabb_dxt.a.x;
        offset = aabb.a.y*size.x + aabb.a.x;
      }

      for (int iy = 0; iy < y/4; iy++) {
        for (int ix = 0; ix < x/4; ix++) {
          auto *dest = reinterpret_cast<unsigned char *>(m_data.data() + offset_dxt + iy*size.x + ix);
          unsigned char src[16];

          memcpy(&src[0], data + iy*4*x + ix*4, 4);
          memcpy(&src[4], data + iy*4*x + ix*4 + 4, 4);
          memcpy(&src[8], data + iy*4*x + ix*4 + 8, 4);
          memcpy(&src[12], data + iy*4*x + ix*4 + 12, 4);

          stb_compress_dxt_block(dest, src, 255, STB_DXT_NORMAL);
        }
      }

      stbi_image_free(data);

      return texture;
    } else {

      stbi_image_free(data);

      return std::nullopt;
    }
  }
  default:stbi_image_free(data);

    return std::nullopt;
  }
}

void ra::renderer::texture_atlas_dxt::debug() {
  spdlog::default_logger()->info("Debug started for texture atlas: {}", reinterpret_cast<std::intptr_t>(this));

  for (auto &texture : m_textures) {
    const auto &aabb = texture->aabb;
    spdlog::default_logger()->info("\t[{}, {}, {}, {}] is {}", aabb.a.x, aabb.a.y, aabb.b.x, aabb.b.y,
                                   texture->free ? "free" : "used");
  }
}

unsigned short ra::renderer::texture_atlas_dxt::percent_free() {
  if (!m_dirty_percent_free)
    return m_cached_percent_free;

  long area_free = 0;

  for (auto &texture : m_textures) {
    if (texture->free)
      area_free += texture->aabb.area();
  }

  m_cached_percent_free = static_cast<unsigned short>(area_free/static_cast<float>(size.x*size.y)*100.0f);
  m_dirty_percent_free = false;

  return m_cached_percent_free;
}

ra::math::vector2u ra::renderer::texture_atlas_dxt::m_pad(const ra::math::vector2u &p_v, unsigned int multiple) {
  ra::math::vector2u remainder = p_v%multiple;

  if (remainder==0)
    return p_v;

  ra::math::vector2u padding = ra::math::vector2u(multiple, multiple) - remainder;
  return size + padding;;
}

bool ra::renderer::texture_atlas_dxt::save_png(const std::string &path) {
  return false;
}

void *ra::renderer::texture_atlas_dxt::data() {
  return m_data.data();
}
