/*
 * This file was created by Magic_RB on 2019-05-18
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_RENDER_BATCH_HPP
#define RA_ENGINE_RENDER_BATCH_HPP

#include <memory>

#include <ra/gl/VAO.hpp>
#include <ra/gl/VBO.hpp>
#include <ra/gl/texture_raw.hpp>

#include "texture_atlas_raw.hpp"
#include "vertex.hpp"

namespace ra::renderer {
struct render_batch {
  ra::gl::VAO vao;
  ra::gl::VBO vbo;
  ra::gl::texture_raw texture_2d;
  std::weak_ptr<ra::renderer::texture_atlas> texture_atlas;
  std::vector<vertex> vertices;
};
}

#endif //RA_ENGINE_RENDER_BATCH_HPP
