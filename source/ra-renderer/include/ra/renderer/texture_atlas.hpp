/*
 * This file was created by Magic_RB on 2019-05-19
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_TEXTURE_ATLAS_HPP
#define RA_ENGINE_TEXTURE_ATLAS_HPP

#include <memory>
#include <optional>
#include <vector>

#include "texture.hpp"
#include "pixel_rgba.hpp"

namespace ra::renderer {
enum texture_type {
  raw,
  dxt5_compressed
};

class texture_atlas {
public:
  [[nodiscard]] virtual std::optional<std::weak_ptr<texture>> allocate(const ra::math::vector2u &p_size) = 0;

  virtual bool deallocate(const std::weak_ptr<texture> &p_texture) = 0;

  virtual bool optimize() = 0;

  virtual std::pair<bool, bool> deallocate_and_optimize(const std::weak_ptr<texture> &p_texture) = 0;

  virtual std::optional<std::weak_ptr<texture>> load(const std::string &path) = 0;

  virtual bool save_png(const std::string &path) = 0;

  virtual void *data() = 0;

  virtual void debug() = 0;

  virtual unsigned short percent_free() = 0;

  ra::math::vector2u size;
  ra::math::vector2u compression_ration;
  texture_type type;
protected:
  bool m_dirty_percent_free = true;
  unsigned short m_cached_percent_free;
private:
};
}

#endif //RA_ENGINE_TEXTURE_ATLAS_HPP
