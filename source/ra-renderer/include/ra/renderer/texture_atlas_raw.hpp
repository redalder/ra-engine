//
// Created by main on 5/10/19.
//

#ifndef RA_ENGINE_TEXTURE_ATLAS_RAW_HPP
#define RA_ENGINE_TEXTURE_ATLAS_RAW_HPP

#include <memory>
#include <optional>
#include <vector>

#include "texture.hpp"
#include "pixel_rgba.hpp"
#include "texture_atlas.hpp"

namespace ra::renderer {
class texture_atlas_raw : public texture_atlas {
public:
  explicit texture_atlas_raw(const ra::math::vector2u &p_size);

  [[nodiscard]] std::optional<std::weak_ptr<texture>> allocate(const ra::math::vector2u &p_size) override;

  bool deallocate(const std::weak_ptr<texture> &p_texture) override;

  bool optimize() override;

  std::pair<bool, bool> deallocate_and_optimize(const std::weak_ptr<texture> &p_texture) override;

  std::optional<std::weak_ptr<texture>> load(const std::string &path) override;

  bool save_png(const std::string &path) override;

  void *data() override;

  void debug() override;

  unsigned short percent_free() override;

protected:
private:
  std::vector<pixel_rgba> m_data;
  std::vector<std::shared_ptr<texture>> m_textures;
};
}

#endif //RA_ENGINE_TEXTURE_ATLAS_RAW_HPP
