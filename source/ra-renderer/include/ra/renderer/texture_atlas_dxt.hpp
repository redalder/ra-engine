//
// Created by main on 5/17/19.
//

#ifndef RA_ENGINE_TEXTURE_ATLAS_DXT_HPP
#define RA_ENGINE_TEXTURE_ATLAS_DXT_HPP

#include <vector>
#include <memory>
#include <optional>

#include <ra/math/vector2.hpp>

#include "ra/renderer/dxt_block.hpp"
#include "ra/renderer/texture.hpp"
#include "texture_atlas.hpp"

namespace ra::renderer {
class texture_atlas_dxt : public texture_atlas {
public:
  explicit texture_atlas_dxt(const ra::math::vector2u &p_size);

  [[nodiscard]] std::optional<std::weak_ptr<texture>> allocate(const ra::math::vector2u &p_size) override;

  bool deallocate(const std::weak_ptr<texture> &p_texture) override;

  bool optimize() override;

  std::pair<bool, bool> deallocate_and_optimize(const std::weak_ptr<texture> &p_texture) override;

  std::optional<std::weak_ptr<texture>> load(const std::string &path) override;

  bool save_png(const std::string &path) override;

  void *data() override;

  void debug() override;

  unsigned short percent_free() override;

  ra::math::vector2u dxt_size;
protected:
private:
  ra::math::vector2u m_pad(const ra::math::vector2u &p_v, unsigned int multiple);

  std::vector<ra::renderer::dxt_block> m_data;
  std::vector<std::shared_ptr<ra::renderer::texture>> m_textures;
};
}

#endif //RA_ENGINE_TEXTURE_ATLAS_DXT_HPP
