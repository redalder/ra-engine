/*
 * This file was created by Magic_RB on 2019-05-19
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_RENDERER_HPP
#define RA_ENGINE_RENDERER_HPP

#include <map>
#include <entt/entity/registry.hpp>
#include <ra/gl/program.hpp>
#include <ra/math/matrix.hpp>

#include "texture_atlas_raw.hpp"
#include "render_batch.hpp"
#include "renderer_create_info.hpp"

namespace ra::renderer {
class renderer {
public:
  explicit renderer(const renderer_create_info &p_create_info);

  void window_resized(int p_width, int p_height);

  void build();

  void render();

  ra::math::vector2f window_size;
protected:
private:
  void m_create_renderer_batch(const std::shared_ptr<ra::renderer::texture_atlas> &p_texture_atlas);

  void m_update_projection(float aspect_ratio);

  std::shared_ptr<ra::ecs::registry> m_registry;
  std::shared_ptr<ra::gl::program> m_program;

  unsigned int m_texture_location;
  unsigned int m_projection_location;
  unsigned int m_translation_location;

  ra::math::matrix_4x4_f m_projection;
  ra::math::matrix_4x4_f m_translation;

  float m_z_near;
  float m_z_far;

  ra::math::vector2f m_visible_tiles;

  std::string m_vertex_shader_str = "\n"
                                    "#version 330 core\n"
                                    "layout (location = 0) in vec3 aPos;\n"
                                    "layout (location = 1) in vec2 aUV;\n"
                                    "\n"
                                    "uniform mat4 projection_matrix;\n"
                                    "uniform mat4 translation_matrix;\n"
                                    "\n"
                                    "out vec2 uv;\n"
                                    "\n"
                                    "void main()\n"
                                    "{\n"
                                    "   gl_Position = projection_matrix * translation_matrix * vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
                                    "   uv = aUV;\n"
                                    "}";
  std::string m_fragment_shader_str = "\n"
                                      "#version 330 core\n"
                                      "\n"
                                      "in vec2 uv;\n"
                                      "\n"
                                      "out vec4 FragColor;\n"
                                      "\n"
                                      "uniform sampler2D in_texture;\n"
                                      "\n"
                                      "void main()\n"
                                      "{\n"
                                      "    FragColor = texture(in_texture, uv);\n"
                                      "}";
  ra::gl::shader m_vertex_shader;
  ra::gl::shader m_fragment_shader;

  std::map<std::shared_ptr<ra::renderer::texture_atlas>, std::shared_ptr<render_batch>> m_atlas_map;
};
}

#endif //RA_ENGINE_RENDERER_HPP
