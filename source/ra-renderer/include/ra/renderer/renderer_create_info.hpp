//
// Created by main on 5/20/19.
//

#ifndef RA_ENGINE_RENDERER_CREATE_INFO_HPP
#define RA_ENGINE_RENDERER_CREATE_INFO_HPP

#include <vector>
#include <memory>

#include <entt/entt.hpp>
#include <ra/ecs/registry.hpp>
#include <ra/gl/program.hpp>

#include "texture_atlas.hpp"

namespace ra::renderer {
struct renderer_create_info {
  std::shared_ptr<ra::ecs::registry> registry;
  ra::math::vector2f window_size;
  ra::math::vector2f visible_tiles;
  float z_near;
  float z_far;
};
}

#endif //RA_ENGINE_RENDERER_CREATE_INFO_HPP
