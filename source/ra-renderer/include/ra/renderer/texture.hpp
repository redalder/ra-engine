//
// Created by main on 5/10/19.
//

#ifndef RA_ENGINE_TEXTURE_HPP
#define RA_ENGINE_TEXTURE_HPP

#include <ra/math/aabb2.hpp>

namespace ra::renderer {
struct texture {
  texture(ra::math::aabb2u p_aabb, bool p_free)
      : aabb(std::move(p_aabb)), free(p_free) {};
  ra::math::aabb2u aabb;
  bool free;
};
}

#endif //RA_ENGINE_TEXTURE_HPP
