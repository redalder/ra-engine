/*
 * This file was created by Magic_RB on 2019-05-19
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_VERTEX_HPP
#define RA_ENGINE_VERTEX_HPP

#include <ra/math/vector3.hpp>
#include <ra/math/vector2.hpp>

namespace ra::renderer {
struct vertex {
  vertex(const ra::math::vector3f &p_position, const ra::math::vector2f &p_uv)
      : position(p_position), uv(p_uv) {}

  ra::math::vector3f position;
  ra::math::vector2f uv;
};
}

#endif //RA_ENGINE_VERTEX_HPP
