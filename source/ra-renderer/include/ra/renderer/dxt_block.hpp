//
// Created by main on 5/17/19.
//

#ifndef RA_ENGINE_DXT_BLOCK_HPP
#define RA_ENGINE_DXT_BLOCK_HPP

namespace ra::renderer {
struct dxt_block {
  unsigned char data[4];
};
}

#endif //RA_ENGINE_DXT_BLOCK_HPP
