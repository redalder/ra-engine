//
// Created by main on 5/10/19.
//

#ifndef RA_ENGINE_PIXEL_RGBA_HPP
#define RA_ENGINE_PIXEL_RGBA_HPP

#include <ra/math/vector4.hpp>

namespace ra::renderer {
using pixel_rgba = ra::math::vector4<unsigned char>;
}

#endif //RA_ENGINE_PIXEL_RGBA_HPP
