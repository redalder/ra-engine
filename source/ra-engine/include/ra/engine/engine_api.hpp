//
// Created by main on 8/8/19.
//

#ifndef RA_ENGINE_SOURCE_RA_ENGINE_INCLUDE_RA_ENGINE_ENGINE_API_HPP
#define RA_ENGINE_SOURCE_RA_ENGINE_INCLUDE_RA_ENGINE_ENGINE_API_HPP

#include <memory>
#include <ra/ecs/registry.hpp>
#include <ra/input/input.hpp>

namespace ra::engine {
class engine_api {
public:
  std::shared_ptr<ra::ecs::registry> registry;
  ra::input::input input;
};
}

#endif //RA_ENGINE_SOURCE_RA_ENGINE_INCLUDE_RA_ENGINE_ENGINE_API_HPP
