//
// Created by main on 8/5/19.
//

#ifndef RA_ENGINE_STATIONARY_HPP
#define RA_ENGINE_STATIONARY_HPP

#include <cstdlib>
#include <optional>

namespace ra::engine {
    template<class T>
    class stationary {
    public:
        stationary()
        {
            m_data = static_cast<T*>(malloc(sizeof(T)));

            if (m_data==nullptr)
                return;
        }

        template<class...Args>
        explicit stationary(Args...args)
        {
            m_data = static_cast<T*>(malloc(sizeof(T)));

            if (m_data==nullptr)
                return;

            *m_data = T(args...);
        }

        std::optional<T&> operator*()
        {
            if (m_data==nullptr)
                return std::nullopt;

            return *m_data;
        }

        const std::optional<T&> operator*() const
        {
            if (m_data==nullptr)
                return std::nullopt;

            return *m_data;
        }

        std::optional<T&> operator->()
        {
            if (m_data==nullptr)
                return std::nullopt;

            return *m_data;
        }

        const std::optional<T&> operator->() const
        {
            if (m_data==nullptr)
                return std::nullopt;

            return *m_data;
        }

        std::optional<T*> data()
        {
            if (m_data==nullptr)
                return std::nullopt;

            return m_data;
        }

    protected:
    private:
        T* m_data;
    };
}

#endif //RA_ENGINE_STATIONARY_HPP
