/*
 * This file was created by Magic_RB on 2019-04-03
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_SPRITE_HPP
#define RA_ENGINE_SPRITE_HPP

#include <rttr/registration>

#include <ra/renderer/texture_atlas_raw.hpp>
#include <ra/renderer/texture.hpp>
#include <ra/math/vector2.hpp>

namespace ra::engine::component {
struct sprite {
  sprite(const std::shared_ptr<ra::renderer::texture_atlas> &p_texture_atlas,
         const std::shared_ptr<ra::renderer::texture> &p_texture) {
    texture_atlas = p_texture_atlas;

    auto u = ra::math::vector2f(
        static_cast<float>(p_texture->aabb.a.x)/static_cast<float>(texture_atlas.lock()->size.x),
        static_cast<float>(p_texture->aabb.b.y)/static_cast<float>(texture_atlas.lock()->size.y));
    auto v = ra::math::vector2f(
        static_cast<float>(p_texture->aabb.b.x)/static_cast<float>(texture_atlas.lock()->size.x),
        static_cast<float>(p_texture->aabb.a.y)/static_cast<float>(texture_atlas.lock()->size.y));
    uv = ra::math::aabb2f(u, v);
  }

  ra::math::aabb2f uv;
  std::weak_ptr<ra::renderer::texture_atlas> texture_atlas;

RTTR_ENABLE()
};
}

#endif //RA_ENGINE_SPRITE_HPP
