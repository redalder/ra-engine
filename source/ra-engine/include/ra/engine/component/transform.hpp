/*
 * This file was created by Magic_RB on 2019-03-31
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_TRANSFORM_HPP
#define RA_ENGINE_TRANSFORM_HPP

#include <rttr/registration>

#include <ra/math/vector3.hpp>
#include <ra/math/vector2.hpp>

namespace ra::engine::component {
class transform {
public:
  transform() = default;
  transform(const ra::math::vector3f& p_position, const ra::math::vector3f& p_rotation, const ra::math::vector3f& p_size);

  ra::math::vector3f position;
  ra::math::vector3f rotation;
  ra::math::vector3f size;

  RTTR_ENABLE()
};
}

#endif //RA_ENGINE_TRANSFORM_HPP
