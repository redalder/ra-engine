/*
 * This file was created by Magic_RB on 2019-03-31
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_WORLD_H
#define RA_ENGINE_WORLD_H

#include <map>

#include <ra/math/vector3.hpp>

#include "chunk.hpp"

namespace ra::engine {
class world {
public:
  world();

  std::shared_ptr<entt::registry> registry;

  void add_entity(entt::entity entity);

protected:
private:
  std::map<ra::math::vector3i, ra::engine::chunk> chunks;
};
}

#endif //RA_ENGINE_WORLD_H
