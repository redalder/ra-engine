/*
 * This file was created by Magic_RB on 2019-03-31
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_CHUNK_H
#define RA_ENGINE_CHUNK_H

#include <array>

#include <entt/entt.hpp>

namespace ra::engine {
class world;

static const int chunk_size = 16;
static const int chunk_height = 5;

class chunk {
  friend world;

public:
protected:
private:
  std::array<std::array<std::array<entt::entity, chunk_height>, chunk_size>, chunk_size> tile_data;
};
}

#endif //RA_ENGINE_CHUNK_H
