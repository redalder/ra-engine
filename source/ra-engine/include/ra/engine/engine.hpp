//
// Created by main on 5/30/19.
//

#ifndef RA_ENGINE_ENGINE_HPP
#define RA_ENGINE_ENGINE_HPP

#include <vector>
#include <memory>
#include <string>

#include <spdlog/spdlog.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <ra/ecs/registry.hpp>
#include <ra/renderer/texture_atlas.hpp>
#include <entt/entity/registry.hpp>
#include <ra/renderer/renderer.hpp>
#include <ra/input/input.hpp>
#include <ra/physics/physics_system.hpp>
#include <ra/scripting/script_engine.hpp>
#include <ra/system/registry.hpp>

#include "engine_create_info.hpp"
#include "engine_api.hpp"

namespace ra::scripting { class script_engine; }

namespace ra::engine {
class engine {
public:
  explicit engine(const engine_create_info &p_create_info);

  ~engine();

  void loop();

  static void window_resized(GLFWwindow *p_window, int width, int height);

  ra::system::registry system_registry;

  std::string window_name;
  ra::renderer::renderer renderer;
  ra::scripting::script_engine script_engine;
  ra::physics::physics_system physics_engine;
  std::vector<std::shared_ptr<ra::renderer::texture_atlas>> texture_atlases;
  ra::engine::engine_api engine_api;

  GLFWwindow *m_window;

  double real_frame_time;

protected:
private:
  double m_last_frame_time;
  double m_this_frame_time;
  double m_longest_delta = 0;
};
}

#endif //RA_ENGINE_ENGINE_HPP
