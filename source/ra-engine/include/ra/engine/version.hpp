/*
 * This file was created by Magic_RB on 2019-02-15
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_VERSION_HPP
#define RA_ENGINE_VERSION_HPP

#ifndef CURRENT_VERSION_MAJOR
#define CURRENT_VERSION_MAJOR 1
#endif

#ifndef CURRENT_VERSION_MINOR
#define CURRENT_VERSION_MINOR 0
#endif

#ifndef CURRENT_VERSION_PATCH
#define CURRENT_VERSION_PATCH 0
#endif

#include <cstdint>
#include <string>
#include <regex>

namespace ra::engine {
struct version {
  uint16_t major;
  uint16_t minor;
  uint16_t patch;

  struct regex {
    static const char *version;
    static const char *major;
    static const char *minor;
    static const char *patch;
  };

  version(const uint16_t &p_major, const uint16_t &p_minor, const uint16_t &p_patch);

  explicit version(const std::string &p_version);

  version();

  std::string to_string() const;

  static version current();
};
}

#endif //RA_ENGINE_VERSION_HPP
