//
// Created by main on 7/21/19.
//

#ifndef RA_ENGINE_SOURCE_RA_ENGINE_INCLUDE_RA_ENGINE_INFO_HPP
#define RA_ENGINE_SOURCE_RA_ENGINE_INCLUDE_RA_ENGINE_INFO_HPP

#include "version.hpp"
namespace ra::engine {
class info {
public:
  static const version engine_version;
  static const std::string engine_triplet;
};
}

#endif //RA_ENGINE_SOURCE_RA_ENGINE_INCLUDE_RA_ENGINE_INFO_HPP
