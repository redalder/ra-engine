//
// Created by main on 5/30/19.
//

#ifndef RA_ENGINE_ENGINE_CREATE_INFO_HPP
#define RA_ENGINE_ENGINE_CREATE_INFO_HPP

#include <string>

#include <ra/math/vector2.hpp>

namespace ra::engine {
struct engine_create_info {
  ra::math::vector2f window_size;
  std::string window_name;
  float z_near;
  float z_far;
  ra::math::vector2f visible_tiles;
  ra::math::vector2f gravity;
};
}

#endif //RA_ENGINE_ENGINE_CREATE_INFO_HPP
