/*
 * This file was created by Magic_RB on 2019-07-21
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>
#define STB_DXT_IMPLEMENTATION
#include <stb_dxt.h>