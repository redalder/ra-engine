/*
 * This file was created by Magic_RB on 2019-03-31
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#include <cmath>

#include <spdlog/spdlog.h>

#include "ra/engine/component/transform.hpp"
#include "ra/engine/world.hpp"

ra::engine::world::world()
    : registry(std::make_shared<entt::registry>()) {

}

void ra::engine::world::add_entity(entt::entity entity) {
  if (registry->has<component::transform>(entity)) {
    const int chunk_size = ra::engine::chunk_size;

    const auto transform1 = registry->get<component::transform>(entity);
    const auto position = transform1.position;

    const auto floored_position = ra::math::vector3i(std::floor(position.x), std::floor(position.y),
                                                     std::floor(position.z));
    const auto chunk_position = ra::math::vector3i(floored_position.x/chunk_size, floored_position.y/chunk_size,
                                                   floored_position.z);
    const auto in_chunk_position = ra::math::vector2u(floored_position.x%chunk_size, floored_position.y%chunk_size);

    const auto micro_step = (position.z - floored_position.z)*ra::engine::chunk_height;

    if (position==floored_position) {
      chunks[chunk_position].tile_data.at(in_chunk_position.x).at(in_chunk_position.y).at(micro_step) = entity;
    }
  }
}