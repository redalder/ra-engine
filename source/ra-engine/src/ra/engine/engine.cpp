//
// Created by main on 5/30/19.
//

#include <glad/glad.h>
#include <fstream>
#include <ra/physics/component/dynamic_body_2d.hpp>
#include <ra/physics/component/static_body_2d.hpp>

#include "ra/engine/engine.hpp"

ra::engine::engine *current_instance;

ra::engine::engine::engine(const engine_create_info &p_engine_create_info)
    :
    window_name(p_engine_create_info.window_name),
    renderer(ra::renderer::renderer_create_info()),
    m_this_frame_time(0),
    physics_engine(ra::physics::physics_system(p_engine_create_info.gravity)) {
  if (!glfwInit()) {
    spdlog::error("GLFW failed to initialize!");
    return;
  }

  m_last_frame_time = glfwGetTime();

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  m_window = glfwCreateWindow(640, 480, "GLFW Window", nullptr, nullptr);
  if (!m_window) {
    spdlog::error("GLFW failed to create window! Error: \"{}\"");
    glfwTerminate();
    return;
  }
  glfwMakeContextCurrent(m_window);
  glfwSwapInterval(1);

  glfwSetWindowSizeCallback(m_window, window_resized);

  if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
    spdlog::info("GLAD failed to initialize!");
    glfwTerminate();
    return;
  }

  auto renderer_create_info = ra::renderer::renderer_create_info();
  renderer_create_info.registry = engine_api.registry;
  renderer_create_info.window_size = p_engine_create_info.window_size;
  renderer_create_info.z_near = p_engine_create_info.z_near;
  renderer_create_info.z_far = p_engine_create_info.z_far;
  renderer_create_info.visible_tiles = p_engine_create_info.visible_tiles;

  renderer = ra::renderer::renderer(renderer_create_info);
}

ra::engine::engine::~engine() {
  glfwSetWindowSizeCallback(m_window, nullptr);
  glfwDestroyWindow(m_window);
  glfwTerminate();
}

void ra::engine::engine::loop() {

  glClearColor(0.0f, 1.0f, 0.0f, 1.0f);

  std::ofstream ofstream;
  ofstream.open("perf.log", std::ofstream::out);

  while (!glfwWindowShouldClose(m_window)) {
    current_instance = this;

    m_this_frame_time = glfwGetTime();
    double delta = m_this_frame_time - m_last_frame_time;
    m_last_frame_time = m_this_frame_time;

    if (delta > m_longest_delta)
      m_longest_delta = delta;

    std::string title = window_name + ": " + std::to_string(1/delta) + " ; " + std::to_string(delta);
    glfwSetWindowTitle(m_window, title.c_str());

    ofstream << 1/delta << " " << delta << "\n";

    physics_engine.run(engine_api, delta);

      auto db_view = registry->view<ra::physics::component::dynamic_body_2d>();

      for (auto& entity : db_view) {
          auto db = registry->get<ra::physics::component::dynamic_body_2d>(entity);
          db.b2_body->GetPosition();
      }

      auto sb_view = registry->view<ra::physics::component::static_body_2d>();

      for (auto& entity : sb_view) {
          auto sb = registry->get<ra::physics::component::static_body_2d>(entity);
          sb.b2_body->GetPosition();
      }

      script_engine.on_tick(this, delta);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

      glClear(static_cast<unsigned int>(GL_COLOR_BUFFER_BIT) | static_cast<unsigned int>(GL_DEPTH_BUFFER_BIT));

    renderer.build();
    renderer.render();

    real_frame_time = glfwGetTime() - m_this_frame_time;

    //spdlog::info("{}", real_frame_time);

    glfwSwapBuffers(m_window);
    glfwPollEvents();

    current_instance = nullptr;
  }

  ofstream.close();
}

void ra::engine::engine::window_resized(GLFWwindow *p_window, int width, int height) {
  if (current_instance)
    current_instance->renderer.window_resized(width, height);
}

