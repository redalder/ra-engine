/*
 * This file was created by Magic_RB on 2019-02-15
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#include "ra/engine/version.hpp"
#include <thread>

const char *ra::engine::version::regex::version = R"(^(\d+).(\d+)-(\d+)$)";
const char *ra::engine::version::regex::major = "^(\\d+)\\.";
const char *ra::engine::version::regex::minor = "\\.(\\d+)-";
const char *ra::engine::version::regex::patch = "-(\\d+)$";

ra::engine::version::version(const uint16_t &p_major, const uint16_t &p_minor, const uint16_t &p_patch)
    : major(p_major), minor(p_minor), patch(p_patch) {}

ra::engine::version::version(const std::string &p_version) {
  if (std::regex_match(p_version, std::regex(regex::version))) {
    std::smatch match;

    std::regex_search(p_version, match, std::regex(regex::major));
    std::string str = match.str();
    major = static_cast<uint16_t>(std::stoi(str.substr(0, str.size() - 1)));

    std::regex_search(p_version, match, std::regex(regex::minor));
    str = match.str();
    minor = static_cast<uint16_t>(std::stoi(str.substr(1, str.size() - 2)));

    std::regex_search(p_version, match, std::regex(regex::patch));
    str = match.str();
    patch = static_cast<uint16_t>(std::stoi(str.substr(1, str.size() - 1)));
  }
}

ra::engine::version::version() {
  major = 0;
  minor = 0;
  patch = 0;
}

std::string ra::engine::version::to_string() const {
  return std::string(
      std::to_string(major) +
          "." +
          std::to_string(minor) +
          "-" +
          std::to_string(patch)
  );
}

ra::engine::version ra::engine::version::current() {
  return version(CURRENT_VERSION_MAJOR, CURRENT_VERSION_MINOR, CURRENT_VERSION_PATCH);
}
