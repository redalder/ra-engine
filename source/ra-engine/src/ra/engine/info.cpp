//
// Created by main on 7/21/19.
//

#include "ra/engine/info.hpp"

namespace ra::engine {
  const version info::engine_version = version(CURRENT_VERSION_MAJOR, CURRENT_VERSION_MINOR, CURRENT_VERSION_PATCH);
  const std::string info::engine_triplet = "TARGET_TRIPLET";
}