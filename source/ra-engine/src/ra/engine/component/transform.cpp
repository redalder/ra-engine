//
// Created by main on 7/24/19.
//

#include "ra/engine/component/transform.hpp"

namespace ra::engine::component {
transform::transform(const ra::math::vector3f &p_position,
                     const ra::math::vector3f &p_rotation,
                     const ra::math::vector3f &p_size) : position(p_position), rotation(p_rotation), size(p_size){}
}

RTTR_REGISTRATION {
  using namespace ra::engine;

  rttr::registration::class_<component::transform>("ra::engine::component::transform").
      constructor<>()
      .constructor<ra::math::vector3f, ra::math::vector3f, ra::math::vector3f>()
      .property("position", &component::transform::position)
      .property("rotation", &component::transform::rotation)
      .property("size", &component::transform::size);
}