//
// Created by main on 7/24/19.
//

#include "ra/engine/component/sprite.hpp"

RTTR_REGISTRATION {
  using namespace ra::engine;
  rttr::registration::class_<component::sprite>("ra::engine::component::sprite")
      .constructor<const std::shared_ptr<ra::renderer::texture_atlas>&, const std::shared_ptr<ra::renderer::texture>&>()
      .property("uv", &component::sprite::uv)
      .property("texture_atlas", &component::sprite::texture_atlas);
}