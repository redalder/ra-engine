/*
 * This file was created by Magic_RB on 2019-04-26
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_SHADER_HPP
#define RA_ENGINE_SHADER_HPP

#include <GL/gl.h>
#include <string>

namespace ra::vk {
class shader {
public:
  shader(const std::string &code);

  ~shader();

protected:
private:
  GLuint m_id;
};
}

#endif //RA_ENGINE_SHADER_HPP
