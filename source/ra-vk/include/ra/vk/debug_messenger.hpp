/*
 * This file was created by Magic_RB on 2019-04-27
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_DEBUG_MESSENGER_HPP
#define RA_ENGINE_DEBUG_MESSENGER_HPP

#include <vulkan/vulkan.h>

#include "debug_msg_info.hpp"

namespace ra::vk {
class debug_messenger {
public:
  debug_messenger(ra::vk::instance p_instance, const ra::vk::debug_msg_info &p_debug_msg_info,
                  std::function<void(message_severity, message_type,
                                     const VkDebugUtilsMessengerCallbackDataEXT *)> p_callback_function);

  void destroy(ra::vk::instance p_instance);

  std::function<void(message_severity, message_type,
                     const VkDebugUtilsMessengerCallbackDataEXT *)> callback_function;
protected:
private:
  VkDebugUtilsMessengerEXT m_debug_messenger = nullptr;
};
}

static VKAPI_ATTR VkBool32
VKAPI_CALL debug_callback(
    VkDebugUtilsMessageSeverityFlagBitsEXT p_message_severity,
    VkDebugUtilsMessageTypeFlagsEXT p_message_type,
    const VkDebugUtilsMessengerCallbackDataEXT *p_callback_data,
    void *p_user_data) {
  auto parent = reinterpret_cast<ra::vk::debug_messenger *>(p_user_data);

  parent->callback_function(static_cast<ra::vk::message_severity>(p_message_severity),
                            static_cast<ra::vk::message_type>(p_message_type), p_callback_data);

  return false;
}

#endif //RA_ENGINE_DEBUG_MESSENGER_HPP
