/*
 * This file was created by Magic_RB on 2019-04-28
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_DEBUG_MSG_INFO_HPP
#define RA_ENGINE_DEBUG_MSG_INFO_HPP

#include <vulkan/vulkan.h>

namespace ra::vk {
enum message_severity : unsigned int {
  verbose = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT,
  info = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT,
  warning = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT,
  error = VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
  max_severity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT
};

enum message_type : unsigned int {
  general = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
  validation = VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT,
  performance = VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
  max_type = VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT
};

struct debug_msg_info {
  debug_msg_info() {
    message_severity = 0;
    message_type = 0;
  }

  VkDebugUtilsMessageSeverityFlagsEXT message_severity;
  VkDebugUtilsMessageTypeFlagsEXT message_type;
};
}

#endif //RA_ENGINE_DEBUG_MSG_INFO_HPP
