/*
 * This file was created by Magic_RB on 2019-04-28
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_PHYSICAL_DEVICE_HPP
#define RA_ENGINE_PHYSICAL_DEVICE_HPP

#include <vulkan/vulkan.h>

namespace ra::vk {
class physical_device {
public:
  explicit physical_device(ra::vk::instance p_instance);

protected:
private:
  VkPhysicalDevice m_physical_device = nullptr;

  uint32_t score_device(VkPhysicalDevice p_device);
};
}

#endif //RA_ENGINE_PHYSICAL_DEVICE_HPP
