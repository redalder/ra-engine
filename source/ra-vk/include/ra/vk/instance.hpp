/*
 * This file was created by Magic_RB on 2019-04-26
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_INSTANCE_HPP
#define RA_ENGINE_INSTANCE_HPP

#include <vulkan/vulkan.h>

#include "application_info.hpp"

namespace ra::vk {
class instance {
public:
  instance(const ra::vk::application_info &p_application_info, const std::vector<std::string> &p_extensions,
           const std::vector<std::string> &p_val_layers);

  void destroy();

  VkInstance raw();

protected:
private:
  VkInstance m_instance = nullptr;
};
}

#endif //RA_ENGINE_INSTANCE_HPP
