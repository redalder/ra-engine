/*
 * This file was created by Magic_RB on 2019-04-26
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_GENERIC_HPP
#define RA_ENGINE_GENERIC_HPP

#include <vector>
#include <string>
#include <vulkan/vulkan.h>

namespace ra::vk {
std::vector<VkExtensionProperties> get_extensions();

std::vector<VkLayerProperties> get_layers();

std::vector<std::string> get_extensions_str();

std::vector<std::string> get_layers_str();
}

#endif //RA_ENGINE_GENERIC_HPP
