/*
 * This file was created by Magic_RB on 2019-04-27
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_APPLICATION_INFO_HPP
#define RA_ENGINE_APPLICATION_INFO_HPP

#include <string>

#include <ra/engine/version.hpp>

namespace ra::vk {
struct application_info {
  std::string application_name;
  std::string engine_name;
  ra::engine::version application_version;
  ra::engine::version engine_version;
};
}

#endif //RA_ENGINE_APPLICATION_INFO_HPP
