/*
 * This file was created by Magic_RB on 2019-04-28
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#include <vector>
#include <spdlog/spdlog.h>

#include "ra/vk/instance.hpp"
#include "ra/vk/physical_device.hpp"

ra::vk::physical_device::physical_device(ra::vk::instance p_instance) {
  uint32_t device_count = 0;

  vkEnumeratePhysicalDevices(p_instance.raw(), &device_count, nullptr);

  if (device_count==0) {
    spdlog::default_logger()->error("Vulkan failed to find any physical devices");
  }

  std::vector<VkPhysicalDevice> physical_devices(device_count);
  std::vector<uint32_t> device_scores;
  device_scores.reserve(device_count);

  size_t highest_score = 0;

  vkEnumeratePhysicalDevices(p_instance.raw(), &device_count, physical_devices.data());

  if (physical_devices.empty()) {
    spdlog::default_logger()->info("No vulkan devices available!");
    return;
  }

  spdlog::default_logger()->info("Available physical vulkan devices:");

  for (auto &device : physical_devices) {
    device_scores.push_back(score_device(device));
    VkPhysicalDeviceProperties device_properties;
    vkGetPhysicalDeviceProperties(physical_devices.at(highest_score), &device_properties);

    spdlog::default_logger()->info("\t{},", device_properties.deviceName);
  }

  for (size_t i = 0; i < device_scores.size(); i++) {
    if (device_scores.at(i) > device_scores.at(highest_score)) {
      highest_score = i;
    }
  }

  VkPhysicalDeviceProperties device_properties;
  vkGetPhysicalDeviceProperties(physical_devices.at(highest_score), &device_properties);

  spdlog::default_logger()->info("Picked device with name '{}'", device_properties.deviceName);
}

uint32_t ra::vk::physical_device::score_device(VkPhysicalDevice p_device) {
  // @TODO figure out a non-hardcoded mechanism

  uint32_t score = 0;

  VkPhysicalDeviceFeatures device_features;
  VkPhysicalDeviceProperties device_properties;
  vkGetPhysicalDeviceFeatures(p_device, &device_features);
  vkGetPhysicalDeviceProperties(p_device, &device_properties);

  switch (device_properties.deviceType) {
  case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:score += 2;
    break;
  case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:score += 1;
    break;
  case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
    //score += 0;
    break;
  case VK_PHYSICAL_DEVICE_TYPE_CPU:score -= 1;
    break;
  default:
    //score += 0;
    break;
  }

  if (device_features.geometryShader) {
    score += 1;
  }

  return score;
}
