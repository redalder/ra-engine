/*
 * This file was created by Magic_RB on 2019-04-27
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#include <spdlog/spdlog.h>

#include "ra/vk/instance.hpp"
#include "ra/vk/debug_messenger.hpp"

ra::vk::debug_messenger::debug_messenger(ra::vk::instance p_instance, const ra::vk::debug_msg_info &p_debug_msg_info,
                                         std::function<void(ra::vk::message_severity, ra::vk::message_type,
                                                            const VkDebugUtilsMessengerCallbackDataEXT *)> p_callback_function)
    : callback_function(std::move(p_callback_function)) {
  VkDebugUtilsMessengerCreateInfoEXT create_info = {};
  create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
  create_info.messageSeverity = p_debug_msg_info.message_severity;
  create_info.messageType = p_debug_msg_info.message_type;
  create_info.pfnUserCallback = debug_callback;
  create_info.pUserData = this;

  auto vk_func = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(p_instance.raw(),
                                                                                            "vkCreateDebugUtilsMessengerEXT"));

  if (vk_func==nullptr) {
    spdlog::default_logger()->warn("Vulkan debug extension not available!");
    return;
  }

  if (vk_func(p_instance.raw(), &create_info, nullptr, &m_debug_messenger)!=VK_SUCCESS) {
    spdlog::default_logger()->warn("Vulkan debug messenger setup failed!");
    return;
  }
}

void ra::vk::debug_messenger::destroy(ra::vk::instance p_instance) {
  auto vk_func = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(p_instance.raw(),
                                                                                             "vkDestroyDebugUtilsMessengerEXT"));

  if (vk_func==nullptr) {
    spdlog::default_logger()->warn("Vulkan debug extension not available!");
    return;
  }

  vk_func(p_instance.raw(), m_debug_messenger, nullptr);
}
