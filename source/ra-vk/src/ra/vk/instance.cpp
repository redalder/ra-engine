/*
 * This file was created by Magic_RB on 2019-04-26
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#include <spdlog/spdlog.h>

#include <ra/engine/version.hpp>

#include "ra/vk/generic.hpp"
#include "ra/vk/instance.hpp"

ra::vk::instance::instance(const ra::vk::application_info &p_application_info,
                           const std::vector<std::string> &p_extensions,
                           const std::vector<std::string> &p_val_layers) {
  const auto available_extensions = get_extensions_str();
  const auto available_layers = get_layers_str();

  if (p_application_info.application_name.empty() || p_application_info.engine_name.empty()) {
    spdlog::default_logger()->warn("Engine name or application name left empty!");
    return;
  }

  std::vector<const char *> extensions;

  for (auto &extension : p_extensions) {
    if (extension.size() > 64) {
      spdlog::default_logger()->warn("Vulkan extension with name: '{}'; name too long!", extension);
      continue;
    } else if (std::find(available_extensions.begin(), available_extensions.end(), extension)
        ==available_extensions.end()) {
      spdlog::default_logger()->warn("Vulkan extension with name: '{}'; not available!", extension);
      continue;
    }

    extensions.push_back(extension.c_str());
  }

  std::vector<const char *> layers;

  for (auto &layer : p_val_layers) {
    if (layer.size() > 64) {
      spdlog::default_logger()->warn("Vulkan layer with name: '{}'; name too long!");
      continue;
    } else if (std::find(available_layers.begin(), available_layers.end(), layer)==available_layers.end()) {
      spdlog::default_logger()->warn("Vulkan layer with name: '{}'; not available!", layer);
      continue;
    }

    layers.push_back(layer.c_str());
  }

  VkApplicationInfo app_info = {};

  app_info.pApplicationName = p_application_info.application_name.c_str();
  app_info.pEngineName = p_application_info.engine_name.c_str();
  app_info.applicationVersion = VK_MAKE_VERSION(p_application_info.application_version.major,
                                                p_application_info.application_version.minor,
                                                p_application_info.application_version.patch);
  app_info.engineVersion = VK_MAKE_VERSION(p_application_info.engine_version.major,
                                           p_application_info.engine_version.minor,
                                           p_application_info.engine_version.patch);
  app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  app_info.apiVersion = VK_API_VERSION_1_1;

  VkInstanceCreateInfo create_info = {};

  create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  create_info.pApplicationInfo = &app_info;
  create_info.enabledExtensionCount = extensions.size();
  create_info.ppEnabledExtensionNames = extensions.data();
  create_info.enabledLayerCount = layers.size();
  create_info.ppEnabledLayerNames = layers.data();

  if (vkCreateInstance(&create_info, nullptr, &m_instance)!=VK_SUCCESS) {
    spdlog::default_logger()->error("Vulkan instance creation failed!");
  }
}

void ra::vk::instance::destroy() {
  vkDestroyInstance(m_instance, nullptr);
}

VkInstance ra::vk::instance::raw() {
  return m_instance;
}
