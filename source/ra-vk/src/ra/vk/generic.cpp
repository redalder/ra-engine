/*
 * This file was created by Magic_RB on 2019-04-26
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#include "ra/vk/generic.hpp"

std::vector<VkExtensionProperties> ra::vk::get_extensions() {
  uint32_t extension_count;

  vkEnumerateInstanceExtensionProperties(nullptr, &extension_count, nullptr);

  std::vector<VkExtensionProperties> layers(extension_count);

  vkEnumerateInstanceExtensionProperties(nullptr, &extension_count, layers.data());

  return layers;
}

std::vector<VkLayerProperties> ra::vk::get_layers() {
  uint32_t layer_count;

  vkEnumerateInstanceLayerProperties(&layer_count, nullptr);

  std::vector<VkLayerProperties> layers(layer_count);

  vkEnumerateInstanceLayerProperties(&layer_count, layers.data());

  return layers;
}

std::vector<std::string> ra::vk::get_extensions_str() {
  const auto extensions = get_extensions();
  std::vector<std::string> extensions_str(extensions.size());

  for (auto &extension : extensions)
    extensions_str.emplace_back(extension.extensionName);

  return extensions_str;
}

std::vector<std::string> ra::vk::get_layers_str() {
  const auto layers = get_layers();
  std::vector<std::string> layers_str(layers.size());

  for (auto &layer : layers)
    layers_str.emplace_back(layer.layerName);

  return layers_str;
}
