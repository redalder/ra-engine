//
// Created by main on 7/24/19.
//

#ifndef RA_ENGINE_SOURCE_RA_MULTIPLAYER_INCLUDE_RA_MULTIPLAYER_PACKET_HPP
#define RA_ENGINE_SOURCE_RA_MULTIPLAYER_INCLUDE_RA_MULTIPLAYER_PACKET_HPP

#include <vector>
#include <cstdint>

namespace ra::multiplayer {
  struct packet {
    enum {
      component_sync
    } type;

    union {
      struct {

      } component_sync;
    } data;

    std::vector<std::uint8_t> serialize() {

    }

    void deserialize (const std::vector<std::uint8_t>& p_data) {

    }
  };
}

#endif //RA_ENGINE_SOURCE_RA_MULTIPLAYER_INCLUDE_RA_MULTIPLAYER_PACKET_HPP
