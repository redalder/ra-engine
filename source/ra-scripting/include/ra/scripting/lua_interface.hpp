//
// Created by main on 7/23/19.
//

#ifndef RA_ENGINE_SOURCE_RA_SCRIPTING_INCLUDE_RA_SCRIPTING_LUA_INTERFACE_HPP
#define RA_ENGINE_SOURCE_RA_SCRIPTING_INCLUDE_RA_SCRIPTING_LUA_INTERFACE_HPP

namespace ra::scripting {
class lua_interface {
public:
  void test();
protected:
private:
};
}

#endif //RA_ENGINE_SOURCE_RA_SCRIPTING_INCLUDE_RA_SCRIPTING_LUA_INTERFACE_HPP
