//
// Created by main on 7/23/19.
//

#ifndef RA_ENGINE_SOURCE_RA_SCRIPTING_INCLUDE_RA_SCRIPTING_SPDLOG_INTERFACE_HPP
#define RA_ENGINE_SOURCE_RA_SCRIPTING_INCLUDE_RA_SCRIPTING_SPDLOG_INTERFACE_HPP

#include <string>

namespace ra::scripting::spdlog_interface {
void info(const std::string &msg);
void warn(const std::string &msg);
void error(const std::string &msg);
}

#endif //RA_ENGINE_SOURCE_RA_SCRIPTING_INCLUDE_RA_SCRIPTING_SPDLOG_INTERFACE_HPP
