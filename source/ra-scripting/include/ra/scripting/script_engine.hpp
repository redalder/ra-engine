//
// Created by main on 8/5/19.
//

#ifndef RA_ENGINE_SCRIPT_ENGINE_HPP
#define RA_ENGINE_SCRIPT_ENGINE_HPP

#include <memory>

namespace ra::engine { class engine; }

namespace ra::scripting {
    class script_engine {
    public:
        void on_tick(ra::engine::engine* p_engine, double p_delta);

    protected:
    private:
    };
}

#endif //RA_ENGINE_SCRIPT_ENGINE_HPP
