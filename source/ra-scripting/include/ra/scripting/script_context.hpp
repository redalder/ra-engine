//
// Created by main on 8/5/19.
//

#ifndef RA_ENGINE_SCRIPT_CONTEXT_HPP
#define RA_ENGINE_SCRIPT_CONTEXT_HPP

#include <ra/engine/engine.hpp>

namespace ra::scripting {
    class script_context {
    public:
        script_context()
                :engine(nullptr), entity() { }

        script_context(ra::engine::engine* p_engine, const entt::entity& p_entity)
                :engine(p_engine), entity(p_entity) { }

        ra::engine::engine* engine;
        entt::entity entity;
    protected:
    private:
    };
}

#endif //RA_ENGINE_SCRIPT_CONTEXT_HPP
