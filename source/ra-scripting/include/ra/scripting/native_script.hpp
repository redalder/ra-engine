//
// Created by main on 8/5/19.
//

#ifndef RA_ENGINE_NATIVE_SCRIPT_HPP
#define RA_ENGINE_NATIVE_SCRIPT_HPP

#include <memory>

#include <ra/engine/engine.hpp>

#include "ra/scripting/script_context.hpp"

namespace ra::scripting {
    class native_script {
    public:
        virtual ~native_script() = default;

        virtual void on_tick(const ra::scripting::script_context& p_script_context, double p_delta) = 0;

        virtual void on_collision_begin(const ra::scripting::script_context& p_script_context, entt::entity other) = 0;

        virtual void on_collision_end(const ra::scripting::script_context& p_script_context, entt::entity other) = 0;
    };
}

#endif //RA_ENGINE_NATIVE_SCRIPT_HPP
