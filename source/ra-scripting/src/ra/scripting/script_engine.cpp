//
// Created by main on 8/5/19.
//

#include <ra/physics/component/dynamic_body_2d.hpp>
#include <ra/physics/component/static_body_2d.hpp>
#include "ra/scripting/script_engine.hpp"

#include "ra/scripting/native_script.hpp"

void ra::scripting::script_engine::on_tick(ra::engine::engine* p_engine, double p_delta)
{
    auto registry = p_engine->engine_api.registry;
    auto view = registry->view<ra::scripting::native_script*>();

    for (auto& entity : view) {
        auto native_script = registry->get<ra::scripting::native_script*>(entity);

        native_script->on_tick(ra::scripting::script_context{p_engine, entity}, p_delta);
    }
}
