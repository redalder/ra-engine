//
// Created by main on 7/23/19.
//

#include <spdlog/spdlog.h>

#include "ra/scripting/spdlog_interface.hpp"

namespace ra::scripting::spdlog_interface {
void info(const std::string &msg) {
  spdlog::info(msg);
}
void warn(const std::string &msg) {
  spdlog::warn(msg);
}
void error(const std::string &msg) {
  spdlog::error(msg);
}
}