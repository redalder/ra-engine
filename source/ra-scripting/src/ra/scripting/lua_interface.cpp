//
// Created by main on 7/23/19.
//

#include <sol/sol.hpp>
#include <spdlog/spdlog.h>

#include "ra/scripting/spdlog_interface.hpp"
#include "ra/scripting/lua_interface.hpp"

namespace ra::scripting {
void lua_interface::test() {
  sol::state lua;
  lua.open_libraries(sol::lib::base);
  lua.set_function("spdlog_4info", ra::scripting::spdlog_interface::info);
  lua.set_function("spdlog_warn", ra::scripting::spdlog_interface::warn);
  lua.set_function("spdlog_error", ra::scripting::spdlog_interface::error);

  lua.script("spdlog_error('test')");
}
}