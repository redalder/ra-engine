//
// Created by main on 7/24/19.
//

#ifndef RA_ENGINE_SOURCE_RA_ECS_INCLUDE_RA_ECS_REGISTRY_HPP
#define RA_ENGINE_SOURCE_RA_ECS_INCLUDE_RA_ECS_REGISTRY_HPP

#include <map>

#include <rttr/type.h>

#include <entt/entt.hpp>

namespace ra::ecs {
class registry {
public:
  entt::entity create() {
    entt::entity entity = m_registry.create();
    m_entity_component_map.emplace(std::pair(entity, std::vector<rttr::type>()));
    return entity;
  }

  void destroy(const entt::entity& p_entity) {
    m_entity_component_map.erase(p_entity);
    m_registry.destroy(p_entity);
  }

    template<typename... Component>
    decltype(auto) has(const entt::entity& p_entity)
    {
        return m_registry.has<Component...>(p_entity);
    }

  template <typename... Component>
  decltype(auto) get(const entt::entity& p_entity) {
    return m_registry.get<Component...>(p_entity);
  }

  template <typename T, typename ...Args>
  void assign(const entt::entity& p_entity, Args... args) {
    if (!m_registry.has<T>(p_entity)) {
      m_entity_component_map.at(p_entity).push_back(rttr::type::get<T>());
      m_registry.assign<T>(p_entity, args...);
    }
  }

  template <typename T>
  void remove(const entt::entity& p_entity) {
    auto& vector = m_entity_component_map.at(p_entity);
    vector.erase(std::remove(vector.begin(), vector.end(), rttr::type::get<T>()), vector.end());
    m_registry.remove<T>(p_entity);
  }

  template <typename ...Args>
  decltype(auto) view() {
    return m_registry.view<Args...>();
  }
protected:
private:
  std::map<entt::entity, std::vector<rttr::type>> m_entity_component_map;
  entt::registry m_registry;
};
}

#endif //RA_ENGINE_SOURCE_RA_ECS_INCLUDE_RA_ECS_REGISTRY_HPP
