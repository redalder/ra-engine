//
// Created by main on 8/5/19.
//

#ifndef RA_ENGINE_CONTACT_LISTNER_HPP
#define RA_ENGINE_CONTACT_LISTNER_HPP

#include <Box2D/Box2D.h>

namespace ra::physics {
    class contact_listner : public b2ContactListener {
    public:
        void BeginContact(b2Contact* contact) override;

        void EndContact(b2Contact* contact) override;

    protected:
    private:
    };
}

#endif //RA_ENGINE_CONTACT_LISTNER_HPP
