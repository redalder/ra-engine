//
// Created by main on 6/12/19.
//

#ifndef RA_ENGINE_DYNAMIC_BODY_2D_HPP
#define RA_ENGINE_DYNAMIC_BODY_2D_HPP

#include <ra/engine/engine.hpp>
#include <ra/math/vector2.hpp>
#include <ra/scripting/script_context.hpp>
#include <ra/engine/stationary.hpp>

#include "body_2d_create_info.hpp"

// @TODO destroy box2d object

namespace ra::physics::component {
class dynamic_body_2d {
public:
  b2Body *b2_body;

    dynamic_body_2d();

    explicit dynamic_body_2d(const body_2d_create_info& p_create_info);

    void set_velocity(const ra::math::vector2f& p_velocity);

  void set_velocity(float p_x, float p_y);

  ra::math::vector2f get_velocity();

    void set_position(const ra::math::vector2f& p_position);

  void set_position(float p_x, float p_y);

  ra::math::vector2f get_position();

    void set_transform(const ra::math::vector2f& p_position, float p_angle);

  void set_angle(float p_angle);

  float get_angle();

protected:
private:
    ra::engine::stationary<ra::scripting::script_context> script_context;
};
}

#endif //RA_ENGINE_DYNAMIC_BODY_2D_HPP
