//
// Created by main on 7/20/19.
//

#ifndef RA_ENGINE_SOURCE_RA_PHYSICS_INCLUDE_RA_PHYSICS_COMPONENT_COLLIDER_COLLIDER_HPP
#define RA_ENGINE_SOURCE_RA_PHYSICS_INCLUDE_RA_PHYSICS_COMPONENT_COLLIDER_COLLIDER_HPP

#include <Box2D/Box2D.h>

namespace ra::physics::component {
class collider {
public:
  virtual b2Shape *box2d() = 0;
protected:
private:
};
}

#endif //RA_ENGINE_SOURCE_RA_PHYSICS_INCLUDE_RA_PHYSICS_COMPONENT_COLLIDER_COLLIDER_HPP
