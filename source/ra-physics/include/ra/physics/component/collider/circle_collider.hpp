//
// Created by main on 6/12/19.
//

#ifndef RA_ENGINE_CIRCLE_COLLIDER_HPP
#define RA_ENGINE_CIRCLE_COLLIDER_HPP

#include <Box2D/Box2D.h>

#include "collider.hpp"

// @TODO destroy box2d object

namespace ra::physics::component {
class circle_collider : public collider {
public:
  circle_collider(float p_radius);

  b2Shape *box2d() override;

  float get_radius();
protected:
private:
  b2CircleShape _b2_circle_shape;
};
}

#endif //RA_ENGINE_CIRCLE_COLLIDER_HPP
