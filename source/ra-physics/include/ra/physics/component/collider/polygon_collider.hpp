//
// Created by main on 7/18/19.
//

#ifndef RA_ENGINE_POLYGON_COLLIDER_HPP
#define RA_ENGINE_POLYGON_COLLIDER_HPP

#include <vector>

#include <Box2D/Box2D.h>

#include <ra/math/vector2.hpp>

#include "collider.hpp"

namespace ra::physics::component {
class polygon_collider : public collider {
public:
  polygon_collider();

  polygon_collider(const std::vector<ra::math::vector2f> &p_points);

  static polygon_collider from_box(const ra::math::vector2f &p_dimensions);

  static polygon_collider from_box(float p_x_dim, float p_y_dim);

  static polygon_collider
  from_box(const ra::math::vector2f &p_dimensions, const ra::math::vector2f &p_origin, float angle);

  static polygon_collider from_box(float p_x_dim, float p_y_dim, const ra::math::vector2f &p_origin, float angle);

  b2Shape *box2d() override;

protected:
private:
  b2PolygonShape _b2_polygon_shape;
};
}

#endif //RA_ENGINE_POLYGON_COLLIDER_HPP
