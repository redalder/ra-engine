//
// Created by main on 7/14/19.
//

#ifndef RA_ENGINE_TOP_DOWN_CREATE_INFO_HPP
#define RA_ENGINE_TOP_DOWN_CREATE_INFO_HPP

#include <Box2D/Box2D.h>
#include <ra/math/vector2.hpp>

namespace ra::physics::component {
struct top_down_create_info {
  b2World *b2_world;
  b2Body *rigid_body;
  b2Body *anchor_body;
  ra::math::vector2f anchor;
  float max_force;
  float max_torque;
};
}

#endif //RA_ENGINE_TOP_DOWN_CREATE_INFO_HPP
