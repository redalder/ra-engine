//
// Created by main on 7/14/19.
//

#ifndef RA_ENGINE_TOP_DOWN_HPP
#define RA_ENGINE_TOP_DOWN_HPP

#include <Box2D/Box2D.h>
#include <ra/math/vector2.hpp>
#include "top_down_create_info.hpp"

namespace ra::physics::component {
class top_down {
public:
  top_down(const top_down_create_info &p_create_info);

protected:
private:
  b2FrictionJoint *_b2_friction_joint;
};
}

#endif //RA_ENGINE_TOP_DOWN_HPP
