//
// Created by main on 7/14/19.
//

#ifndef RA_ENGINE_STATIC_BODY_2D_CREATE_INFO_HPP
#define RA_ENGINE_STATIC_BODY_2D_CREATE_INFO_HPP

#include <Box2D/Box2D.h>
#include <entt/entt.hpp>

#include <ra/engine/component/transform.hpp>

#include <ra/physics/component/collider/collider.hpp>

namespace ra::physics::component {
struct body_2d_create_info {
  std::shared_ptr<b2World> b2_world;
  ra::engine::component::transform transform;
  ra::physics::component::collider *collider;
  float friction;
  float density;
    ra::engine::engine* engine;
    entt::entity entity;
};
}

#endif //RA_ENGINE_STATIC_BODY_2D_CREATE_INFO_HPP
