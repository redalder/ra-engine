//
// Created by main on 6/12/19.
//

#ifndef RA_PHYSICS_PHYSICS_ENGINE_HPP
#define RA_PHYSICS_PHYSICS_ENGINE_HPP

namespace ra::engine { class engine; }

#include <memory>

#include <entt/entt.hpp>
#include <Box2D/Box2D.h>
#include <ra/math/clamp.hpp>
#include <ra/system/system.hpp>

#include "contact_listner.hpp"

namespace ra::physics {
class physics_system : public ra::system::system {
public:
    explicit physics_system(const ra::math::vector2f& p_gravity);

    void run(const std::shared_ptr<ra::engine::engine_api>& p_engine, float delta) override;
  bool request_speedup() override;
  bool allow_slowdown() override;
  double rough_speed() const override;

  std::shared_ptr<b2World> b2_world;
protected:
private:
    ra::physics::contact_listner contact_listner;

  ra::math::clamp<int> m_velocity_iterations = ra::math::clamp<int>(0, 15, 7);
  ra::math::clamp<int> m_position_iterations = ra::math::clamp<int>(0, 7, 2);
};
}

#endif //RA_PHYSICS_PHYSICS_ENGINE_HPP
