//
// Created by main on 6/12/19.
//

#include <cmath>

#include <ra/physics/component/collider/circle_collider.hpp>
#include <ra/physics/component/dynamic_body_2d.hpp>
#include <ra/engine/component/transform.hpp>
#include <spdlog/spdlog.h>

#include "ra/physics/physics_system.hpp"

ra::physics::physics_system::physics_system(const ra::math::vector2f& p_gravity)
{
  b2_world = std::make_shared<b2World>(p_gravity.to_b2());
  b2_world->SetContactListener(&contact_listner);
}

void ra::physics::physics_system::run(const std::shared_ptr<ra::engine::engine_api> &p_engine, float delta) {
  auto view = p_engine->registry->view<ra::engine::component::transform, ra::physics::component::dynamic_body_2d>();

  for (auto entity : view) {
    auto &transform = p_engine->registry->get<ra::engine::component::transform>(entity);
    auto &rigid_body_2d = p_engine->registry->get<ra::physics::component::dynamic_body_2d>(entity);

    if (transform.position.to_vector2()!=rigid_body_2d.get_position()
        || transform.rotation.y!=rigid_body_2d.get_angle()) {
      rigid_body_2d.set_transform(transform.position.to_vector2(), transform.rotation.y);
    }
  }

  b2_world->Step(delta, m_velocity_iterations.raw(), m_position_iterations.raw());

  for (auto entity : view) {
    auto &transform = p_engine->registry->get<ra::engine::component::transform>(entity);
    auto &rigid_body_2d = p_engine->registry->get<ra::physics::component::dynamic_body_2d>(entity);

    if (transform.position.to_vector2()!=rigid_body_2d.get_position()
        || transform.rotation.y!=rigid_body_2d.get_angle()) {
      transform.position = ra::math::vector3f(rigid_body_2d.get_position(), transform.position.z);
      transform.rotation.y = rigid_body_2d.get_angle();
    }
  }
}
bool ra::physics::physics_system::request_speedup() {
  m_velocity_iterations--;
  m_position_iterations--;

  return true;
}
bool ra::physics::physics_system::allow_slowdown() {
  m_velocity_iterations++;
  m_position_iterations++;

  return true;
}
double ra::physics::physics_system::rough_speed() const {
  return 0;
}
