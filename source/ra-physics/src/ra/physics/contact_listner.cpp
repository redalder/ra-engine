//
// Created by main on 8/5/19.
//

#include <ra/scripting/script_context.hpp>
#include <ra/scripting/native_script.hpp>

#include "ra/physics/contact_listner.hpp"

void ra::physics::contact_listner::BeginContact(b2Contact* contact)
{
    auto context1 = static_cast<ra::scripting::script_context*>(contact->GetFixtureA()->GetUserData());
    auto context2 = static_cast<ra::scripting::script_context*>(contact->GetFixtureB()->GetUserData());

    ra::engine::engine* engine_ptr;

    if (context1==nullptr || context2==nullptr)
        return;

    if (context1->engine!=nullptr)
        engine_ptr = context1->engine;
    else if (context2->engine!=nullptr)
        engine_ptr = context2->engine;
    else return;

    if (engine_ptr->engine_api.registry->has<ra::scripting::native_script*>(context1->entity)) {
        auto native_script1 = engine_ptr->engine_api.registry->get<ra::scripting::native_script*>(context1->entity);

        native_script1->on_collision_begin(*context1, context2->entity);
    }

    if (engine_ptr->engine_api.registry->has<ra::scripting::native_script*>(context2->entity)) {
        auto native_script2 = engine_ptr->engine_api.registry->get<ra::scripting::native_script*>(context2->entity);

        native_script2->on_collision_begin(*context2, context1->entity);
    }
}

void ra::physics::contact_listner::EndContact(b2Contact* contact)
{
    auto context1 = static_cast<ra::scripting::script_context*>(contact->GetFixtureA()->GetUserData());
    auto context2 = static_cast<ra::scripting::script_context*>(contact->GetFixtureB()->GetUserData());

    if (context1==nullptr || context2==nullptr || context1->engine==nullptr)
        return;

    if (context1->engine->engine_api.registry->has<ra::scripting::native_script*>(context1->entity)) {
        auto native_script1 = context1->engine->engine_api.registry->get<ra::scripting::native_script*>(context1->entity);

        native_script1->on_collision_end(*context1, context2->entity);
    }

    if (context1->engine->engine_api.registry->has<ra::scripting::native_script*>(context2->entity)) {
        auto native_script2 = context1->engine->engine_api.registry->get<ra::scripting::native_script*>(context2->entity);

        native_script2->on_collision_end(*context2, context1->entity);
    }
}
