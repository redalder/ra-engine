//
// Created by main on 7/14/19.
//

#include "ra/physics/component/top_down.hpp"

// @TODO destroy box2d object

ra::physics::component::top_down::top_down(const top_down_create_info &p_create_info) {
  b2FrictionJointDef b2_friction_joint_def;
  b2_friction_joint_def.Initialize(p_create_info.rigid_body, p_create_info.anchor_body,
                                   p_create_info.rigid_body->GetWorldCenter());

  b2_friction_joint_def.maxForce = p_create_info.max_force;
  b2_friction_joint_def.maxTorque = p_create_info.max_torque;

  _b2_friction_joint = dynamic_cast<b2FrictionJoint *>(p_create_info.b2_world->CreateJoint(&b2_friction_joint_def));
}
