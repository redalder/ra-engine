//
// Created by main on 7/14/19.
//

#include <ra/engine/component/transform.hpp>
#include <ra/physics/component/collider/circle_collider.hpp>
#include "ra/physics/component/static_body_2d.hpp"

// @TODO destroy box2d object

ra::physics::component::static_body_2d::static_body_2d(const body_2d_create_info& p_create_info)
{
  auto &transform = p_create_info.transform;

    script_context = ra::engine::stationary<ra::scripting::script_context>(p_create_info.engine, p_create_info.entity);

  b2BodyDef b2_body_def;

  b2_body_def.position = b2Vec2(transform.position.x, transform.position.y);
  b2_body_def.angle = transform.rotation.y;

  b2_body = p_create_info.b2_world->CreateBody(&b2_body_def);

  auto collider = p_create_info.collider;

  b2FixtureDef b2_fixture_def;
  b2_fixture_def.shape = collider->box2d();
    b2_fixture_def.userData = script_context.data().value();

  b2_body->CreateFixture(&b2_fixture_def);
}

void ra::physics::component::static_body_2d::set_position(const ra::math::vector2f& p_position)
{
  b2_body->SetTransform(p_position.to_b2(), b2_body->GetAngle());
}

void ra::physics::component::static_body_2d::set_position(float p_x, float p_y) {
  b2_body->SetTransform(b2Vec2(p_x, p_y), b2_body->GetAngle());
}

ra::math::vector2f ra::physics::component::static_body_2d::get_position() {
  return ra::math::vector2f::from_b2(b2_body->GetPosition());
}

void ra::physics::component::static_body_2d::set_transform(const ra::math::vector2f& p_position, float p_angle)
{
  b2_body->SetTransform(p_position.to_b2(), p_angle);
}

void ra::physics::component::static_body_2d::set_angle(float p_angle) {
  b2_body->SetTransform(b2_body->GetPosition(), p_angle);
}

float ra::physics::component::static_body_2d::get_angle() {
  return b2_body->GetAngle();
}
