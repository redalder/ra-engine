//
// Created by main on 7/18/19.
//

#include "ra/physics/component/collider/polygon_collider.hpp"

namespace ra::physics::component {
polygon_collider::polygon_collider() {

}

polygon_collider::polygon_collider(const std::vector<ra::math::vector2f> &p_points) {
  std::vector<b2Vec2> points;
  points.reserve(p_points.size());

  for (auto &point : p_points) {
    points.emplace_back(point.x, point.y);
  }

  _b2_polygon_shape.Set(points.data(), points.size());
}

polygon_collider polygon_collider::from_box(const ra::math::vector2f &p_dimensions) {
  ra::physics::component::polygon_collider collider;

  collider._b2_polygon_shape.SetAsBox(p_dimensions.x, p_dimensions.y);

  return collider;
}

polygon_collider polygon_collider::from_box(float p_x_dim, float p_y_dim) {
  polygon_collider collider;

  collider._b2_polygon_shape.SetAsBox(p_x_dim, p_y_dim);

  return collider;
}

polygon_collider polygon_collider::from_box(const ra::math::vector2f &p_dimensions,
                                            const ra::math::vector2f &p_origin, float angle) {
  polygon_collider collider;

  collider._b2_polygon_shape.SetAsBox(p_dimensions.x, p_dimensions.y, p_origin.to_b2(), angle);

  return collider;
}

polygon_collider polygon_collider::from_box(float p_x_dim, float p_y_dim, const ra::math::vector2f& p_origin,
                                            float angle) {
  polygon_collider collider;

  collider._b2_polygon_shape.SetAsBox(p_x_dim, p_y_dim, p_origin.to_b2(), angle);

  return collider;
}

b2Shape *polygon_collider::box2d() {
  return &_b2_polygon_shape;
}
}