//
// Created by main on 7/14/19.
//

#include "ra/physics/component/collider/circle_collider.hpp"

namespace ra::physics::component {
circle_collider::circle_collider(float p_radius) {
  _b2_circle_shape.m_radius = p_radius;
}

b2Shape *circle_collider::box2d() {
  return &_b2_circle_shape;
}

float circle_collider::get_radius() {
  return _b2_circle_shape.m_radius;
}
}