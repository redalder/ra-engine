add_executable(ra-engine-test src/main.cpp include/player.hpp src/player.cpp)
target_link_libraries(ra-engine-test PUBLIC ra-engine)
target_include_directories(ra-engine-test PUBLIC include PRIVATE src)

target_compile_options(ra-engine-test PRIVATE -Wall -Wextra -pedantic)