
/*
 * This file was created by Magic_RB on 2019-06-06
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_PLAYER_HPP
#define RA_ENGINE_PLAYER_HPP

#include <ra/engine/engine.hpp>
#include <ra/scripting/native_script.hpp>

class player : public ra::scripting::native_script {
public:
  player(entt::entity p_entity, ra::engine::engine *p_engine, std::weak_ptr<ra::renderer::texture> p_texture,
         std::shared_ptr<ra::renderer::texture_atlas> p_texture_atlas);

    ~player() override;

    void on_tick(const ra::scripting::script_context&, double delta) override;

    void on_collision_begin(const ra::scripting::script_context& p_script_context, entt::entity other) override;

    void on_collision_end(const ra::scripting::script_context& p_script_context, entt::entity other) override;

protected:
private:
  entt::entity m_entity;
  ra::engine::engine *m_engine;
  std::weak_ptr<ra::renderer::texture> m_texture;
  std::shared_ptr<ra::renderer::texture_atlas> m_texture_atlas;
  bool m_p_pressed;
  bool m_b_pressed;
  ra::math::vector2f movement_delta;
  std::array<double, 60> m_frame_times;
  int m_frame_times_index;
};

#endif //RA_ENGINE_PLAYER_HPP
