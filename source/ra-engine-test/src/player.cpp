/*
 * This file was created by Magic_RB on 2019-06-06
 *
 * It is licensed under the MIT license, which can be found in full at \${PROJECT_ROOT}/LICENSE
 */

#include <spdlog/spdlog.h>
#include <ra/engine/component/transform.hpp>
#include <ra/engine/component/sprite.hpp>
#include <ra/physics/component/dynamic_body_2d.hpp>

#include "player.hpp"

player::player(entt::entity p_entity, ra::engine::engine *p_engine, std::weak_ptr<ra::renderer::texture> p_texture,
               std::shared_ptr<ra::renderer::texture_atlas> p_texture_atlas)
    : m_entity(p_entity), m_engine(p_engine), m_texture(std::move(p_texture)),
      m_texture_atlas(std::move(p_texture_atlas)) {
  m_p_pressed = false;
  m_b_pressed = false;

  m_frame_times_index = -1;
}

player::~player()
{

}

// @TODO deregister this once the object is destroyed

void player::on_tick(const ra::scripting::script_context&, double delta)
{
  auto &transform = m_engine->registry->get<ra::engine::component::transform>(m_entity);
  auto &rigid_body_2d = m_engine->registry->get<ra::physics::component::dynamic_body_2d>(m_entity);

  if (m_engine->input.get_key_state(ra::input::a) || m_engine->input.get_key_state(ra::input::left)) {
    rigid_body_2d.set_velocity(-4.0f, rigid_body_2d.get_velocity().y);
    movement_delta = ra::math::vector2f(-0.1f, 0.0f);
  }
  if (m_engine->input.get_key_state(ra::input::d) || m_engine->input.get_key_state(ra::input::right)) {
    rigid_body_2d.set_velocity(4.0f, rigid_body_2d.get_velocity().y);
    movement_delta = ra::math::vector2f(0.1f, 0.0f);
  }
  if (m_engine->input.get_key_state(ra::input::s) || m_engine->input.get_key_state(ra::input::down)) {
    rigid_body_2d.set_velocity(rigid_body_2d.get_velocity().x, -4.0f);
    movement_delta = ra::math::vector2f(0.0f, -0.1f);
  }
  if (m_engine->input.get_key_state(ra::input::w) || m_engine->input.get_key_state(ra::input::up)) {
    rigid_body_2d.set_velocity(rigid_body_2d.get_velocity().x, 4.0f);
    movement_delta = ra::math::vector2f(0.0f, 0.1f);
  }
  if (m_engine->input.get_key_state(ra::input::p) && !m_p_pressed) {
    m_p_pressed = !m_p_pressed;
  }
  if (!m_engine->input.get_key_state(ra::input::p) && m_p_pressed) {
    m_p_pressed = !m_p_pressed;
  }
  if (m_engine->input.get_key_state(ra::input::b) && !m_b_pressed) {
    m_b_pressed = !m_b_pressed;

    m_frame_times_index = 0;
  }
  if (!m_engine->input.get_key_state(ra::input::b) && m_b_pressed) {
    m_b_pressed = !m_b_pressed;
  }
  if (m_frame_times_index >= 0) {
    m_frame_times[m_frame_times_index] = m_engine->real_frame_time;
    m_frame_times_index++;

    if (static_cast<std::size_t>(m_frame_times_index)==m_frame_times.size() - 1) {
      m_frame_times_index = -1;
      double arithmetic_average = 0.0;
      for (auto &frame_time : m_frame_times) {
        arithmetic_average += frame_time;
      }
      arithmetic_average /= m_frame_times.size();
      spdlog::default_logger()->info("{}", arithmetic_average);
    }
  }

  //spdlog::default_logger()->info("{}", m_engine->real_frame_time);

  movement_delta = ra::math::vector2f(0.0f, 0.0f);
}

void player::on_collision_begin(const ra::scripting::script_context& p_script_context, entt::entity other)
{
    spdlog::info("Collision!");
}

void player::on_collision_end(const ra::scripting::script_context& p_script_context, entt::entity other)
{
    spdlog::info("Nollision!");
}
