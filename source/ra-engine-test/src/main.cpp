/*
 * This file was created by Magic_RB on 2019-02-15
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <cstdio>
#include <spdlog/spdlog.h>
#include <entt/entt.hpp>

#include <ra/math/aabb2.hpp>
#include <ra/engine/world.hpp>
#include <ra/engine/component/transform.hpp>
#include <ra/engine/component/sprite.hpp>
#include <ra/gl/shader.hpp>
#include <ra/gl/program.hpp>
#include <ra/renderer/renderer.hpp>
#include <ra/renderer/texture_atlas_dxt.hpp>
#include <ra/math/matrix.hpp>
#include <ra/resource/resource.hpp>
#include <player.hpp>
#include <ra/input/input.hpp>
#include <ra/engine/engine.hpp>
#include <ra/physics/component/dynamic_body_2d.hpp>
#include <ra/physics/component/collider/circle_collider.hpp>
#include <ra/physics/component/static_body_2d.hpp>
#include <ra/physics/component/top_down.hpp>
#include <ra/physics/component/collider/polygon_collider.hpp>
#include <ra/engine/info.hpp>
#include <ra/scripting/lua_interface.hpp>

int main(int argv, char **args) {
  ra::engine::engine_create_info engine_create_info;
  engine_create_info.window_size = ra::math::vector2f(640.0f, 480.0f);
  engine_create_info.window_name = "Hello from ra-engine!";
  engine_create_info.z_far = 100.0f;
  engine_create_info.z_near = 0.0f;
  engine_create_info.visible_tiles = ra::math::vector2f(15.0f, 15.0f);

  ra::engine::engine engine(engine_create_info);

  // TESTING AREA BEGIN

  engine.texture_atlases.push_back(std::make_shared<ra::renderer::texture_atlas_raw>(ra::math::vector2u(4096, 4096)));
  auto texture_atlas = engine.texture_atlases.at(engine.texture_atlases.size() - 1);
  auto texture = texture_atlas->load(ra::resource::get_asset_dir() + "textures/red-adler-new.png").value();
  auto texture1 = texture_atlas->load(ra::resource::get_asset_dir() + "textures/Ground_01.png").value();
  auto texture2 = texture_atlas->load(ra::resource::get_asset_dir() + "textures/Ground_01_Nrm.png").value();

  auto player_entity = engine.registry->create();
  {
    engine.registry->assign<ra::engine::component::transform>(player_entity,
                                                              ra::math::vector3f(0.0f, 0.0f, 0.0f),
                                                              ra::math::vector3f(0.0f, 0.0f, 0.0f),
                                                              ra::math::vector3f(1.0f, 1.0f, 0.0f));
    engine.registry->assign<ra::engine::component::sprite>(player_entity, texture_atlas, texture.lock());
    engine.registry->assign<player>(player_entity, player_entity, &engine, texture2, texture_atlas);
      engine.registry->assign<ra::scripting::native_script*>(player_entity,
              &engine.registry->get<player>(player_entity));
    engine.registry->assign<ra::physics::component::polygon_collider>(player_entity,
                                                                      ra::physics::component::polygon_collider::from_box(
                                                                          ra::math::vector2f(0.5f, 0.5f)));

    ra::physics::component::body_2d_create_info db_info = {};
    db_info.b2_world = engine.physics_engine.b2_world;
    db_info.transform = engine.registry->get<ra::engine::component::transform>(player_entity);
    db_info.collider = &engine.registry->get<ra::physics::component::polygon_collider>(player_entity);
    db_info.density = 1.0f;
    db_info.friction = 1.0f;
      db_info.engine = &engine;
      db_info.entity = player_entity;

    engine.registry->assign<ra::physics::component::dynamic_body_2d>(player_entity, db_info);
  }

    auto ball_entity = engine.registry->create();
    {
        engine.registry->assign<ra::engine::component::transform>(ball_entity, ra::math::vector3f(0.0f, 0.0f, 0.0f),
                ra::math::vector3f(0.0f, 0.0f, 0.0f),
                ra::math::vector3f(0.5f, 0.5f, 0.0f));
        engine.registry->assign<ra::engine::component::sprite>(ball_entity, texture_atlas, texture2.lock());
        engine.registry->assign<ra::physics::component::circle_collider>(ball_entity, 0.25f);

        ra::physics::component::body_2d_create_info db_info = {};
        db_info.b2_world = engine.physics_engine.b2_world;
        db_info.transform = engine.registry->get<ra::engine::component::transform>(ball_entity);
        db_info.collider = &engine.registry->get<ra::physics::component::circle_collider>(ball_entity);
        db_info.density = 1.0f;
        db_info.friction = 1.0f;
        db_info.engine = &engine;
        db_info.entity = ball_entity;

        engine.registry->assign<ra::physics::component::dynamic_body_2d>(ball_entity, db_info);
    }

  auto wall_entity = engine.registry->create();
  {
    engine.registry->assign<ra::engine::component::transform>(wall_entity,
                                                              ra::math::vector3f(0.0f, 0.0f, 0.0f),
                                                              ra::math::vector3f(0.0f, 0.0f, 0.0f),
                                                              ra::math::vector3f(1.0f, 5.0f, 0.0f));
    engine.registry->assign<ra::engine::component::sprite>(wall_entity, texture_atlas, texture1.lock());
    engine.registry->assign<ra::physics::component::polygon_collider>(wall_entity,
                                                                      ra::physics::component::polygon_collider::from_box(
                                                                          ra::math::vector2f(0.5f, 2.5f)));

    ra::physics::component::body_2d_create_info sb_info = {};
    sb_info.transform = engine.registry->get<ra::engine::component::transform>(wall_entity);
    sb_info.b2_world = engine.physics_engine.b2_world;
    sb_info.collider = &engine.registry->get<ra::physics::component::polygon_collider>(wall_entity);
      sb_info.engine = &engine;
      sb_info.entity = wall_entity;

    engine.registry->assign<ra::physics::component::static_body_2d>(wall_entity, sb_info);
  }

  auto ground_box = engine.registry->create();
  {
    engine.registry->assign<ra::engine::component::transform>(ground_box,
                                                              ra::math::vector3f(0.0f, 0.0f, 0.0f),
                                                              ra::math::vector3f(0.0f, 0.0f, 0.0f),
                                                              ra::math::vector3f(0.0f, 0.0f, 0.0f));
    engine.registry->assign<ra::physics::component::polygon_collider>(ground_box,
                                                                      ra::physics::component::polygon_collider::from_box(
                                                                          ra::math::vector2f(100.f, 100.0f)));

    ra::physics::component::body_2d_create_info sb_info = {};
    sb_info.transform = engine.registry->get<ra::engine::component::transform>(ground_box);
    sb_info.b2_world = engine.physics_engine.b2_world;
    sb_info.collider = &engine.registry->get<ra::physics::component::polygon_collider>(ground_box);
      sb_info.engine = &engine;
      sb_info.entity = ground_box;

    engine.registry->assign<ra::physics::component::static_body_2d>(ground_box, sb_info);
  }
  {
    ra::physics::component::top_down_create_info td_info = {};
    td_info.b2_world = engine.physics_engine.b2_world.get();
    td_info.anchor = ra::math::vector2f(0.0f, 0.0f);
    td_info.rigid_body = engine.registry->get<ra::physics::component::dynamic_body_2d>(player_entity).b2_body;
    td_info.anchor_body = engine.registry->get<ra::physics::component::static_body_2d>(ground_box).b2_body;
    td_info.max_force = 10.0f;
    td_info.max_torque = 10.0f;

    engine.registry->assign<ra::physics::component::top_down>(player_entity, td_info);
  }
    {
        ra::physics::component::top_down_create_info td_info = {};
        td_info.b2_world = engine.physics_engine.b2_world.get();
        td_info.anchor = ra::math::vector2f(0.0f, 0.0f);
        td_info.rigid_body = engine.registry->get<ra::physics::component::dynamic_body_2d>(ball_entity).b2_body;
        td_info.anchor_body = engine.registry->get<ra::physics::component::static_body_2d>(ground_box).b2_body;
        td_info.max_force = 10.0f;
        td_info.max_torque = 10.0f;

        engine.registry->assign<ra::physics::component::top_down>(ball_entity, td_info);
    }

  spdlog::info("Running on {} with version {}", ra::engine::info::engine_triplet, ra::engine::info::engine_version.to_string());

  ra::scripting::lua_interface lua;
  lua.test();

  auto transform_type = rttr::type::get<ra::engine::component::transform>();
  auto direct_type = engine.registry->get<ra::engine::component::transform>(wall_entity);
  spdlog::info("should be the same as the next triple [{}, {}, {}]", direct_type.size.x, direct_type.size.y, direct_type.size.z);
  auto rttr_transform = transform_type.get_property_value("size", direct_type);
  if (rttr_transform.can_convert<ra::math::vector3f>()) {
    auto size = rttr_transform.get_value<ra::math::vector3f>();
    spdlog::info("[{}, {}, {}]", size.x, size.y, size.z);
  }

  engine.loop();
  // TESTING AREA END
}
