//
// Created by main on 5/8/19.
//

#include "ra/gl/VAO.hpp"

ra::gl::VAO::VAO() = default;

ra::gl::VAO::~VAO() {
  glDeleteVertexArrays(1, &m_buffer_id);
}

void ra::gl::VAO::gen() {
  if (m_buffer_id)
    return;

  glGenVertexArrays(1, &m_buffer_id);
}

void ra::gl::VAO::bind() {
  glBindVertexArray(m_buffer_id);
}
