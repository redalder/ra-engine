//
// Created by main on 5/10/19.
//

#include "ra/gl/texture_raw.hpp"

ra::gl::texture_raw::texture_raw() = default;

ra::gl::texture_raw::texture_raw(void *p_data, const ra::math::vector2u &p_size) {
  glGenTextures(1, &m_texture_id);

  glBindTexture(GL_TEXTURE_2D, m_texture_id);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, static_cast<int>(p_size.x), static_cast<int>(p_size.y), 0, GL_RGBA,
               GL_UNSIGNED_BYTE, p_data);
}

ra::gl::texture_raw::~texture_raw() {
  glDeleteTextures(1, &m_texture_id);
}

void ra::gl::texture_raw::gen() {
  if (m_texture_id)
    return;

  glGenTextures(1, &m_texture_id);
}

void ra::gl::texture_raw::set_data(void *p_data, const ra::math::vector2u &p_size) {
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  glBindTexture(GL_TEXTURE_2D, m_texture_id);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, static_cast<int>(p_size.x), static_cast<int>(p_size.y), 0, GL_RGBA,
               GL_UNSIGNED_BYTE, p_data);
  glGenerateMipmap(GL_TEXTURE_2D);
}

void ra::gl::texture_raw::bind() {
  glBindTexture(GL_TEXTURE_2D, m_texture_id);
}
