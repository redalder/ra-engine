//
// Created by main on 5/8/19.
//

#include <spdlog/spdlog.h>

#include "ra/gl/program.hpp"

//@TODO Make move only

ra::gl::program::program(const ra::gl::shader &p_vertex_shader, const ra::gl::shader &p_fragment_shader) {
  m_program_id = glCreateProgram();

  glAttachShader(m_program_id, p_vertex_shader.shader_id);

  spdlog::info("{}", glGetError());

  glAttachShader(m_program_id, p_fragment_shader.shader_id);

  spdlog::info("{}", glGetError());

  glLinkProgram(m_program_id);

  spdlog::info("{}", glGetError());

  int success;
  char info_log[512];

  glGetProgramiv(m_program_id, GL_LINK_STATUS, &success);

  if (!success) {
    glGetProgramInfoLog(m_program_id, sizeof(info_log), nullptr, info_log);
    spdlog::warn("Program linking failed with error: \"{}\"", info_log);
  }
}

ra::gl::program::~program() {
  glDeleteProgram(m_program_id);
}

void ra::gl::program::use() {
  glUseProgram(m_program_id);
}

unsigned int ra::gl::program::get_location(const std::string &p_location) {
  return glGetUniformLocation(m_program_id, p_location.c_str());
}
