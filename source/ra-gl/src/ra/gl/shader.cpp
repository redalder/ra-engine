//
// Created by main on 5/8/19.
//

#include <spdlog/spdlog.h>

#include "ra/gl/shader.hpp"

//@TODO Make move only

ra::gl::shader::shader()
    : shader_id(0) {};

ra::gl::shader::shader(GLenum p_shader_type, const std::string &p_shader_code) {
  shader_id = glCreateShader(p_shader_type);

  const char *shader_source = p_shader_code.c_str();

  glShaderSource(shader_id, 1, &shader_source, nullptr);
  glCompileShader(shader_id);

  int success;
  char info_log[512];
  glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(shader_id, sizeof(info_log), nullptr, info_log);
    spdlog::warn("Shader compilation failed with: \"{}\"", info_log);
  }
}

ra::gl::shader::~shader() {
  if (!shader_id)
    return;
  glDeleteShader(shader_id);
}

ra::gl::shader::shader(ra::gl::shader &&p_other) {
  shader_id = p_other.shader_id;
  p_other.shader_id = 0;
}

ra::gl::shader &ra::gl::shader::operator=(ra::gl::shader &&p_other) {
  shader_id = p_other.shader_id;
  p_other.shader_id = 0;
  return *this;
}
