//
// Created by main on 5/8/19.
//

#include <spdlog/spdlog.h>

#include "glad/glad.h"
#include "ra/gl/VBO.hpp"

ra::gl::VBO::VBO() = default;

void ra::gl::VBO::gen() {
  if (m_buffer_id)
    return;

  glGenBuffers(1, &m_buffer_id);
}

ra::gl::VBO::VBO(void *p_data, std::size_t p_size) {
  glGenBuffers(1, &m_buffer_id);

  glBindBuffer(GL_ARRAY_BUFFER, m_buffer_id);
  glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(p_size), p_data, GL_STATIC_DRAW);
}

ra::gl::VBO::~VBO() {
  glDeleteBuffers(1, &m_buffer_id);
}

void ra::gl::VBO::set_data(void *p_data, std::size_t p_size) {
  glBindBuffer(GL_ARRAY_BUFFER, m_buffer_id);
  glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(p_size), p_data, GL_STATIC_DRAW);
}

void ra::gl::VBO::bind() {
  glBindBuffer(GL_ARRAY_BUFFER, m_buffer_id);
}
