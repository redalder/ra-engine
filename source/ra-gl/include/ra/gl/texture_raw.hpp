//
// Created by main on 5/10/19.
//

#ifndef RA_ENGINE_TEXTURE_RAW_HPP
#define RA_ENGINE_TEXTURE_RAW_HPP

#include <ra/math/vector2.hpp>
#include <glad/glad.h>

namespace ra::gl {
class texture_raw {
public:
  texture_raw();

  texture_raw(void *p_data, const ra::math::vector2u &p_size);

  ~texture_raw();

  texture_raw(texture_raw &&) = default;

  texture_raw &operator=(texture_raw &&) = default;

  texture_raw(const texture_raw &) = delete;

  texture_raw &operator=(const texture_raw &) = delete;

  void gen();

  void set_data(void *p_data, const ra::math::vector2u &p_size);

  void bind();

protected:
private:
  GLuint m_texture_id = 0;
};
}

#endif //RA_ENGINE_TEXTURE_RAW_HPP
