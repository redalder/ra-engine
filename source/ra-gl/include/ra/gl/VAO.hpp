//
// Created by main on 5/8/19.
//

#ifndef RA_ENGINE_VAO_HPP
#define RA_ENGINE_VAO_HPP

#include <cstdio>

#include "glad/glad.h"

namespace ra::gl {
class VAO {
public:
  VAO();

  ~VAO();

  VAO(VAO &&) = default;

  VAO &operator=(VAO &&) = default;

  VAO(const VAO &) = delete;

  VAO &operator=(const VAO &) = delete;

  void bind();

  void gen();

protected:
private:
  GLuint m_buffer_id = 0;
};
}

#endif //RA_ENGINE_VAO_HPP
