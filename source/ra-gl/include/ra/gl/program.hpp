//
// Created by main on 5/8/19.
//

#ifndef RA_ENGINE_PROGRAM_HPP
#define RA_ENGINE_PROGRAM_HPP

#include "shader.hpp"

namespace ra::gl {
class program {
public:
  program(const shader &p_vertex_shader, const shader &p_fragment_shader);

  ~program();

  void use();

  program(program &&) = default;

  program &operator=(program &&) = default;

  program(const program &) = delete;

  program &operator=(const program &) = delete;

  unsigned int get_location(const std::string &p_location);

protected:
private:
  GLuint m_program_id;
};
}

#endif //RA_ENGINE_PROGRAM_HPP
