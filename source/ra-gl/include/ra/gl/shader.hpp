//
// Created by main on 5/8/19.
//

#ifndef RA_ENGINE_SHADER_HPP
#define RA_ENGINE_SHADER_HPP

#include <string>

#include "glad/glad.h"

namespace ra::gl {
class shader {
public:
  shader();

  shader(GLenum p_shader_type, const std::string &p_shader_code);

  ~shader();

  shader(shader &&p_other);

  shader &operator=(shader &&p_other);

  shader(const shader &p_other) = delete;

  shader &operator=(const shader &p_other) = delete;

  GLuint shader_id;
protected:
private:
};
}

#endif //RA_ENGINE_SHADER_HPP
