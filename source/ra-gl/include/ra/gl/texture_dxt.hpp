//
// Created by main on 5/10/19.
//

#ifndef RA_ENGINE_TEXTURE_DXT_HPP
#define RA_ENGINE_TEXTURE_DXT_HPP

#include <ra/math/vector2.hpp>
#include <glad/glad.h>

namespace ra::gl {
class texture_dxt {
public:
  texture_dxt();

  texture_dxt(void *p_data, const ra::math::vector2u &p_size);

  ~texture_dxt();

  texture_dxt(texture_dxt &&) = default;

  texture_dxt &operator=(texture_dxt &&) = default;

  texture_dxt(const texture_dxt &) = delete;

  texture_dxt &operator=(const texture_dxt &) = delete;

  void gen();

  void set_data(void *p_data, const ra::math::vector2u &p_size);

  void bind();

protected:
private:
  GLuint m_texture_id = 0;
};
}

#endif //RA_ENGINE_TEXTURE_DXT_HPP
