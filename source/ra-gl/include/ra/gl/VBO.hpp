//
// Created by main on 5/8/19.
//

#ifndef RA_ENGINE_VBO_HPP
#define RA_ENGINE_VBO_HPP

#include <cstdio>

namespace ra::gl {
class VBO {
public:
  VBO();

  VBO(void *p_data, std::size_t p_size);

  ~VBO();

  VBO(VBO &&) = default;

  VBO &operator=(VBO &&) = default;

  VBO(const VBO &) = delete;

  VBO &operator=(const VBO &) = delete;

  void gen();

  void set_data(void *p_data, std::size_t p_size);

  void bind();

protected:
private:
  GLuint m_buffer_id = 0;
};
}

#endif //RA_ENGINE_VBO_HPP
