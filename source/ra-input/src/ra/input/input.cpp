//
// Created by main on 5/30/19.
//

#include <GLFW/glfw3.h>

#include "ra/input/input.hpp"

ra::input::input::input(GLFWwindow *p_window)
    : m_window(p_window) {

}

bool ra::input::input::get_key_state(ra::input::key_code p_code) {
  return glfwGetKey(m_window, p_code);
}
