//
// Created by main on 5/30/19.
//

#ifndef RA_ENGINE_INPUT_HPP
#define RA_ENGINE_INPUT_HPP

#include <memory>
#include <vector>
#include <functional>

#include <GLFW/glfw3.h>

#include "key_code.hpp"
#include "key_action.hpp"
#include "key_mod.hpp"

namespace ra::input {
class input {
public:
  input() = default;

  explicit input(GLFWwindow *p_window);

  bool get_key_state(key_code p_code);

protected:
  GLFWwindow *m_window;
private:
};
}

#endif //RA_ENGINE_INPUT_HPP
