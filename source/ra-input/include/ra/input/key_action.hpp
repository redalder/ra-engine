//
// Created by main on 5/30/19.
//

#ifndef RA_ENGINE_KEY_ACTION_HPP
#define RA_ENGINE_KEY_ACTION_HPP

namespace ra::input {
enum key_action : int {
  release = 0,
  press = 1,
  repeat = 2
};
}

#endif //RA_ENGINE_KEY_ACTION_HPP
