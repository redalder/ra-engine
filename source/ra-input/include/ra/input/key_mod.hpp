//
// Created by main on 5/30/19.
//

#ifndef RA_ENGINE_KEY_MOD_HPP
#define RA_ENGINE_KEY_MOD_HPP

namespace ra::input {
enum key_mod : int {
  shift = 0x0001,
  control = 0x0002,
  alt = 0x0004,
  super = 0x0008,
  caps_lock_mod = 0x0010,
  num_lock_mod = 0x0020
};
}

#endif //RA_ENGINE_KEY_MOD_HPP
