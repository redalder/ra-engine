/*
 * This file was created by Magic_RB on 2019-03-31
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_VECTOR3_H
#define RA_ENGINE_VECTOR3_H

#include <Box2D/Box2D.h>

namespace ra::math {
template<class T>
struct vector2;

template<class T>
struct vector3 {
  vector3(T p_x, T p_y, T p_z)
      : x(p_x), y(p_y), z(p_z) {}

  vector3(vector2<T> p_vector2, float p_z)
      : x(p_vector2.x), y(p_vector2.y), z(p_z) {}

  vector3() {
    x = 0;
    y = 0;
    z = 0;
  }

  union {
    T x;
    T r;
    T u;
  };

  union {
    T y;
    T g;
    T v;
  };

  union {
    T z;
    T b;
  };

  vector3(const vector3 &other) {
    x = other.x;
    y = other.y;
    z = other.z;
  }

  template<class U>
  vector3 operator+(const U &other) const {
    auto vector = vector3(*this);

    vector.x += other;
    vector.y += other;
    vector.z += other;

    return vector;
  }

  template<class U>
  vector3 operator-(const U &other) const {
    auto vector = vector3(*this);

    vector.x -= other;
    vector.y -= other;
    vector.z -= other;

    return vector;
  }

  template<class U>
  vector3 operator*(const U &other) const {
    auto vector = vector3(*this);

    vector.x *= other;
    vector.y *= other;
    vector.z *= other;

    return vector;
  }

  template<class U>
  vector3 operator/(const U &other) const {
    auto vector = vector3(*this);

    vector.x /= other;
    vector.y /= other;
    vector.z /= other;

    return vector;
  }

  template<class U>
  vector3 operator%(const U &other) const {
    auto vector = vector3(*this);

    vector.x %= other;
    vector.y %= other;
    vector.z %= other;

    return vector;
  }

  template<class U>
  vector3 operator+(const vector3<U> &other) const {
    auto vector = vector3(*this);

    vector.x += other.x;
    vector.y += other.y;
    vector.z += other.z;

    return vector;
  }

  template<class U>
  vector3 operator-(const vector3<U> &other) const {
    auto vector = vector3(*this);

    vector.x -= other.x;
    vector.y -= other.y;
    vector.z -= other.z;

    return vector;
  }

  template<class U>
  vector3 operator*(const vector3<U> &other) const {
    auto vector = vector3(*this);

    vector.x *= other.x;
    vector.y *= other.y;
    vector.z *= other.z;

    return vector;
  }

  template<class U>
  vector3 operator/(const vector3<U> &other) const {
    auto vector = vector3(*this);

    vector.x /= other.x;
    vector.y /= other.y;
    vector.z /= other.z;

    return vector;
  }

  template<class U>
  vector3 operator%(const vector3<U> &other) const {
    auto vector = vector3(*this);

    vector.x %= other.x;
    vector.y %= other.y;
    vector.z %= other.z;

    return vector;
  }

  template<class U>
  bool operator>(const U &&other) const {
    return x > other && y > other && z > other;
  }

  template<class U>
  bool operator<(const U &&other) const {
    return x < other && y < other && z < other;
  }

  template<class U>
  bool operator>=(const U &&other) const {
    return x >= other && y >= other && z >= other;
  }

  template<class U>
  bool operator<=(const U &&other) const {
    return x <= other && y <= other && z <= other;
  }

  template<class U>
  bool operator==(const U &&other) const {
    return x==other && y==other && z==other;
  }

  template<class U>
  bool operator!=(const U &&other) const {
    return x!=other || y!=other || z!=other;
  }

  // -----------------------------------------------

  template<class U>
  bool operator>(const vector3<U> &other) const {
    return x > other.x && y > other.y && z > other.z;
  }

  template<class U>
  bool operator<(const vector3<U> &other) const {
    return x < other.x && y < other.y && z < other.z;
  }

  template<class U>
  bool operator>=(const vector3<U> &other) const {
    return x >= other.x && y >= other.y && z >= other.z;
  }

  template<class U>
  bool operator<=(const vector3<U> &other) const {
    return x <= other.x && y <= other.y && z <= other.z;
  }

  template<class U>
  bool operator==(const vector3<U> &other) const {
    return x==other.x && y==other.y && z==other.z;
  }

  template<class U>
  bool operator!=(const vector3<U> &other) const {
    return x!=other.x || y!=other.y || z!=other.z;
  }

  // -----------------------------------------------

  template<class U>
  bool operator>(const vector3<U> &&other) const {
    return x > other.x && y > other.y && z > other.z;
  }

  template<class U>
  bool operator<(const vector3<U> &&other) const {
    return x < other.x && y < other.y && z < other.z;
  }

  template<class U>
  bool operator>=(const vector3<U> &&other) const {
    return x >= other.x && y >= other.y && z >= other.z;
  }

  template<class U>
  bool operator<=(const vector3<U> &&other) const {
    return x <= other.x && y <= other.y && z <= other.z;
  }

  template<class U>
  bool operator==(const vector3<U> &&other) const {
    return x==other.x && y==other.y && z==other.z;
  }

  template<class U>
  bool operator!=(const vector3<U> &&other) const {
    return x!=other.x || y!=other.y || z!=other.z;
  }

  vector3 &operator=(const vector3 &other) {
    x = other.x;
    y = other.y;

    return *this;
  }

  b2Vec3 to_b2() const {
    return b2Vec3(x, y, z);
  }

  static vector3<T> from_b2(const b2Vec3 &b2_vec3) {
    return vector3<T>(b2_vec3.x, b2_vec3.y, b2_vec3.z);
  }

  vector2<T> to_vector2() {
    return vector2<T>(x, y);
  }
};

typedef vector3<unsigned int> vector3u;
typedef vector3<int> vector3i;
typedef vector3<long> vector3l;
typedef vector3<float> vector3f;
typedef vector3<double> vector3d;
}

#endif //RA_ENGINE_VECTOR3_H
