//
// Created by main on 5/20/19.
//

#ifndef RA_ENGINE_MATRIX_HPP
#define RA_ENGINE_MATRIX_HPP

#include <array>

#include <spdlog/spdlog.h>

#include "vector3.hpp"

namespace ra::math {
template<class T, unsigned int X, unsigned int Y>
struct matrix {
  std::array<T, X*Y> data;

  explicit matrix(std::array<T, X*Y> p_data) {
    data = p_data;
  }

  matrix() {
    data.fill(0);
  }

  void debug() {
    std::string str = "\n";

    for (std::size_t i = 0; i < X*Y; i++) {
      if (i%X==0)
        str += "\t";
      if (i%X!=3)
        str += std::to_string(data[i]) + ", ";
      else
        str += std::to_string(data[i]) + "\n";
    }

    spdlog::default_logger()->info("{}", str);
  }

  static const matrix<T, X, Y> identity() {
    return matrix<T, X, Y>({
                               1, 0, 0, 0,
                               0, 1, 0, 0,
                               0, 0, 1, 0,
                               0, 0, 0, 1});
  }

  static const matrix<float, 4, 4>
  orthographic_projection(float left, float right, float bottom, float top, float z_near, float z_far) {
    return matrix<float, 4, 4>({
                                   2/(right - left), 0, 0, (-(right + left)/(right - left)),
                                   0, 2/(top - bottom), 0, (-(top + bottom)/(top - bottom)),
                                   0, 0, -2/(z_far - z_near), (-(z_far + z_near)/(z_far - z_near)),
                                   0, 0, 0, 1
                               });
  }

  static const matrix<float, 4, 4> translation(const ra::math::vector3f &p_translate) {
    return matrix<float, 4, 4>({
                                   1, 0, 0, p_translate.x,
                                   0, 1, 0, p_translate.y,
                                   0, 0, 1, p_translate.z,
                                   0, 0, 0, 1
                               });
  }

  template<class C>
  matrix<T, X, Y> operator+(const matrix<C, X, Y> &other) const {
    auto result = ra::math::matrix<T, X, Y>();

    for (std::size_t i = 0; i < X*Y; i++) {
      result.data[i] = this->data[i] + other.data[i];
    }

    return result;
  }

  template<class C>
  matrix<T, X, Y> operator-(const matrix<C, X, Y> &other) const {
    auto result = ra::math::matrix<T, X, Y>();

    for (std::size_t i = 0; i < X*Y; i++) {
      result.data[i] = this->data[i] - other.data[i];
    }

    return result;
  }

  template<class C>
  matrix<T, X, Y> operator*(const matrix<C, X, Y> &other) const {
    auto result = ra::math::matrix<T, X, Y>();

    for (std::size_t i = 0; i < X*Y; i++) {
      T sum = 0;

      std::size_t x = i%X;
      std::size_t y = i/X;

      for (std::size_t j = 0; j < X; j++) {
        sum += this->data[y*X + j]*other.data[x + j*X];
      }

      result.data[i] = sum;
    }

    return result;
  }
};

using matrix_4x4_f = matrix<float, 4, 4>;
}

#endif //RA_ENGINE_MATRIX_HPP
