/*
 * This file was created by Magic_RB on 2019-04-05
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_AABB_HPP
#define RA_ENGINE_AABB_HPP

#include "vector2.hpp"

namespace ra::math {
template<class T>
struct aabb2 {
  aabb2(T p_1, T p_2, T p_3, T p_4)
      : a(vector2<T>(p_1, p_2)), b(vector2<T>(p_3, p_4)) {}

  aabb2(vector2<T> p_a, vector2<T> p_b)
      : a(p_a), b(p_b) {}

  aabb2(vector2<T> p_a, T p_3, T p_4)
      : a(p_a), b(vector2<T>(p_3, p_4)) {}

  aabb2(T p_1, T p_2, vector2<T> p_b)
      : a(vector2<T>(p_1, p_2)), b(p_b) {}

  aabb2() {
    a.x = 0;
    a.y = 0;
    b.x = 0;
    b.y = 0;
  }

  vector2<T> size() const {
    if (a.x > b.x || a.y > b.y)
      return vector2<T>(0, 0);
    return (b - a) + 1;
  }

  long area() const {
    vector2<T> size = this->size();

    return size.x*size.y;
  }

  vector2<T> c() const {
    return vector2<T>(b.x, a.y);
  }

  vector2<T> d() const {
    return vector2<T>(a.x, b.y);
  }

  vector2<T> a;
  vector2<T> b;

  aabb2 operator+(const aabb2 &other) const {
    return vector2<T>(a + other.a, b + other.b);
  }

  aabb2 operator-(const aabb2 &other) const {
    return vector2<T>(a - other.a, b - other.b);
  }

  aabb2 operator*(const aabb2 &other) const {
    return vector2<T>(a*other.a, b*other.b);
  }

  aabb2 operator/(const aabb2 &other) const {
    return vector2<T>(a/other.a, b/other.b);
  }

  aabb2 operator%(const aabb2 &other) const {
    return vector2<T>(a%other.a, b%other.b);
  }

  bool operator==(const aabb2 &other) const {
    return a==other.a && b==other.b;
  }

  bool operator!=(const aabb2 &other) const {
    return a!=other.a || b!=other.b;
  }

  bool operator>(const aabb2 &other) const {
    return a > other.a && b > other.b;
  }

  bool operator<(const aabb2 &other) const {
    return a < other.a && b < other.b;
  }

  bool operator>=(const aabb2 &other) const {
    return a >= other.a && b >= other.b;
  }

  bool operator<=(const aabb2 &other) const {
    return a <= other.a && b <= other.b;
  }

  // @TODO finally figure out what universal refs do...

  aabb2 operator+(const T &&other) {
    return aabb2<T>(a + other, b + other);
  }

  aabb2 operator-(const T &&other) {
    return aabb2<T>(a - other, b - other);
  }

  aabb2 operator*(const T &&other) {
    return aabb2<T>(a*other, b*other);
  }

  aabb2 operator/(const T &&other) {
    return aabb2<T>(a/other, b/other);
  }

  aabb2 operator%(const T &&other) {
    return aabb2<T>(a%other, b%other);
  }

  bool operator==(const T &other) {
    return a==other && b==other;
  }

  bool operator!=(const T &other) {
    return a!=other || b!=other;
  }

  bool operator<(const T &other) {
    return a < other && b < other;
  }

  bool operator>(const T &other) {
    return a > other && b > other;
  }

  bool operator<=(const T &other) {
    return a <= other && b <= other;
  }

  bool operator>=(const T &other) {
    return a >= other && b >= other;
  }
};

typedef aabb2<unsigned int> aabb2u;
typedef aabb2<int> aabb2i;
typedef aabb2<long> aabb2l;
typedef aabb2<float> aabb2f;
typedef aabb2<double> aabb2d;
}

#endif //RA_ENGINE_AABB_HPP
