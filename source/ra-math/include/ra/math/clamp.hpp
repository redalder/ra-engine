//
// Created by main on 8/8/19.
//

#ifndef RA_ENGINE_SOURCE_RA_MATH_INCLUDE_RA_MATH_CLAMP_HPP
#define RA_ENGINE_SOURCE_RA_MATH_INCLUDE_RA_MATH_CLAMP_HPP

namespace ra::math {
template <typename T>
class clamp {
public:
  clamp(T p_min, T p_max, T p_num) : m_min(p_min), m_max(p_max) {
    if (p_num > m_max) {
      m_num = m_max;
    } else if (p_num < m_min) {
      m_num = m_min;
    } else {
      m_num = p_num;
    }
  }
  
  clamp(const clamp<T>& other) {
    m_num = other.m_num;
    m_max = other.m_max;
    m_min = other.m_min;
  }

  T raw() {
    return m_num;
  }

  clamp<T>& operator=(const clamp<T>& other) {
    m_num = other.m_num;
    m_max = other.m_max;
    m_min = other.m_min;
  }

  clamp<T>& operator=(const T&& p_num) {
    if (p_num <= m_min && p_num >= m_max) {
      m_num = p_num;
    }
    return *this;
  }

  clamp<T>& operator+(const clamp<T>& other) {
    if (m_num + other.m_num <= m_max) {
      m_num += other.m_num;
    }
    return *this;
  }

  clamp<T>& operator+(const T& p_num) {
    if (m_num + p_num <= m_max) {
      m_num += p_num;
    }
    return *this;
  }

  clamp<T>& operator-(const clamp<T>& other) {
    if (m_num - other.m_num >= m_min) {
      m_num -= other.m_num;
    }
    return *this;
  }

  clamp<T>& operator-(const T& p_num) {
    if (m_num - p_num >= m_min) {
      m_num -= p_num;
    }
    return *this;
  }

  clamp<T>& operator++() {
    if (m_num + 1 <= m_max) {
      m_num++;
    }
    return *this;
  }

  clamp<T>& operator--() {
    if (m_num - 1 >= m_min) {
      m_num--;
    }
    return *this;
  }

  clamp<T> operator++(T) {
    clamp<T> copy = *this;

    ++(*this);

    return *this;
  }

  clamp<T> operator--(T) {
    clamp<T> copy = *this;

    --(*this);

    return copy;
  }
private:
  T m_min;
  T m_max;
  T m_num;
};
}

#endif //RA_ENGINE_SOURCE_RA_MATH_INCLUDE_RA_MATH_CLAMP_HPP
