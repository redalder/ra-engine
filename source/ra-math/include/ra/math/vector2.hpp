/*
 * This file was created by Magic_RB on 2019-03-29
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_VECTOR2_HPP
#define RA_ENGINE_VECTOR2_HPP

#include <Box2D/Box2D.h>

namespace ra::math {
template<class T>
struct vector3;

template<class T>
struct vector2 {
  vector2(T p_x, T p_y)
      : x(p_x), y(p_y) {}

  vector2() {
    x = 0;
    y = 0;
  }

  union {
    T x;
    T r;
    T u;
  };

  union {
    T y;
    T g;
    T v;
  };

  vector2(const vector2 &other) {
    x = other.x;
    y = other.y;
  }

  template<class U>
  vector2 operator+(const U &other) const {
    auto vector = vector2(*this);

    vector.x += other;
    vector.y += other;

    return vector;
  }

  template<class U>
  vector2 operator-(const U &other) const {
    auto vector = vector2(*this);

    vector.x -= other;
    vector.y -= other;

    return vector;
  }

  template<class U>
  vector2 operator*(const U &other) const {
    auto vector = vector2(*this);

    vector.x *= other;
    vector.y *= other;

    return vector;
  }

  template<class U>
  vector2 operator/(const U &other) const {
    auto vector = vector2(*this);

    vector.x /= other;
    vector.y /= other;

    return vector;
  }

  template<class U>
  vector2 operator%(const U &other) const {
    auto vector = vector2(*this);

    vector.x %= other;
    vector.y %= other;

    return vector;
  }

  template<class U>
  vector2 operator+(const vector2<U> &other) const {
    auto vector = vector2(*this);

    vector.x += other.x;
    vector.y += other.y;

    return vector;
  }

  template<class U>
  vector2 operator-(const vector2<U> &other) const {
    auto vector = vector2(*this);

    vector.x -= other.x;
    vector.y -= other.y;

    return vector;
  }

  template<class U>
  vector2 operator*(const vector2<U> &other) const {
    auto vector = vector2(*this);

    vector.x *= other.x;
    vector.y *= other.y;

    return vector;
  }

  template<class U>
  vector2 operator/(const vector2<U> &other) const {
    auto vector = vector2(*this);

    vector.x /= other.x;
    vector.y /= other.y;

    return vector;
  }

  template<class U>
  vector2 operator%(const vector2<U> &other) const {
    auto vector = vector2(*this);

    vector.x %= other.x;
    vector.y %= other.y;

    return vector;
  }

  template<class U>
  bool operator>(const U &&other) const {
    return x > other && y > other;
  }

  template<class U>
  bool operator<(const U &&other) const {
    return x < other && y < other;
  }

  template<class U>
  bool operator>=(const U &&other) const {
    return x >= other && y >= other;
  }

  template<class U>
  bool operator<=(const U &&other) const {
    return x <= other && y <= other;
  }

  template<class U>
  bool operator==(const U &&other) const {
    return x==other && y==other;
  }

  template<class U>
  bool operator!=(const U &&other) const {
    return x!=other || y!=other;
  }

  // -----------------------------------------------

  template<class U>
  bool operator>(const vector2<U> &other) const {
    return x > other.x && y > other.y;
  }

  template<class U>
  bool operator<(const vector2<U> &other) const {
    return x < other.x && y < other.y;
  }

  template<class U>
  bool operator>=(const vector2<U> &other) const {
    return x >= other.x && y >= other.y;
  }

  template<class U>
  bool operator<=(const vector2<U> &other) const {
    return x <= other.x && y <= other.y;
  }

  template<class U>
  bool operator==(const vector2<U> &other) const {
    return x==other.x && y==other.y;
  }

  template<class U>
  bool operator!=(const vector2<U> &other) const {
    return x!=other.x || y!=other.y;
  }

  // -----------------------------------------------

  template<class U>
  bool operator>(const vector2<U> &&other) const {
    return x > other.x && y > other.y;
  }

  template<class U>
  bool operator<(const vector2<U> &&other) const {
    return x < other.x && y < other.y;
  }

  template<class U>
  bool operator>=(const vector2<U> &&other) const {
    return x >= other.x && y >= other.y;
  }

  template<class U>
  bool operator<=(const vector2<U> &&other) const {
    return x <= other.x && y <= other.y;
  }

  template<class U>
  bool operator==(const vector2<U> &&other) const {
    return x==other.x && y==other.y;
  }

  template<class U>
  bool operator!=(const vector2<U> &&other) const {
    return x!=other.x || y!=other.y;
  }

  vector2 &operator=(const vector2 &other) {
    x = other.x;
    y = other.y;

    return *this;
  }

  b2Vec2 to_b2() const {
    return b2Vec2(x, y);
  }

  static vector2<T> from_b2(const b2Vec2 &b2_vec2) {
    return vector2<T>(b2_vec2.x, b2_vec2.y);
  }
};

using vector2u = vector2<unsigned int>;
using vector2i = vector2<int>;
using vector2l = vector2<long>;
using vector2f = vector2<float>;
using vector2d = vector2<double>;
}

#endif //RA_ENGINE_VECTOR2_HPP
