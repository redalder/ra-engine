/*
 * This file was created by Magic_RB on 2019-03-31
 *
 * It is licensed under the MIT license, which can be found in full at ${PROJECT_ROOT}/LICENSE
 */

#ifndef RA_ENGINE_VECTOR4_H
#define RA_ENGINE_VECTOR4_H

namespace ra::math {
template<class T>
struct vector4 {
  vector4(T p_x, T p_y, T p_z, T p_w)
      : x(p_x), y(p_y), z(p_z), w(p_w) {}

  vector4() {
    x = 0;
    y = 0;
    z = 0;
    w = 0;
  }

  union {
    T x;
    T r;
    T u;
  };

  union {
    T y;
    T g;
    T v;
  };

  union {
    T z;
    T b;
  };

  union {
    T w;
    T a;
  };

  template<class C>
  vector4<T> operator+(const vector4<C> &other) {
    return vector4<T>(x + other.x, y + other.y, z + other.z, w + other.w);
  }

  template<class C>
  vector4<T> operator-(const vector4<C> &other) {
    return vector4<T>(x - other.x, y - other.y, z - other.z, w - other.w);
  }

  template<class C>
  vector4<T> operator*(const vector4<C> &other) {
    return vector4<T>(x*other.x, y*other.y, z*other.z, w*other.w);
  }

  template<class C>
  vector4<T> operator/(const vector4<C> &other) {
    return vector4<T>(x/other.x, y/other.y, z/other.z, w/other.w);
  }

  template<class C>
  vector4<T> operator%(const vector4<C> &other) {
    return vector4<T>(x%other.x, y%other.y, z%other.z, w%other.w);
  }

  template<class C>
  bool operator==(const vector4<C> &other) const {
    return x==other.x && y==other.y && z==other.z && w==other.w;
  }

  template<class C>
  bool operator!=(const vector4<C> &other) const {
    return x!=other.x || y!=other.y || z!=other.z || w!=other.w;
  }

  template<class C>
  bool operator>(const vector4<C> &other) const {
    return x > other.x || y > other.y || z > other.z || w > other.w;
  }

  template<class C>
  bool operator<(const vector4<C> &other) const {
    return x < other.x || y < other.y || z < other.z || w < other.w;
  }

  template<class C>
  bool operator<=(const vector4<C> &other) const {
    return x <= other.x || y <= other.y || z <= other.z || w <= other.w;
  }

  template<class C>
  bool operator>=(const vector4<C> &other) const {
    return x >= other.x || y >= other.y || z >= other.z || w >= other.w;
  }

  // @TODO finally figure out what universal refs do...

  vector4<T> operator+(const T &&other) {
    return vector4<T>(x + other, y + other, z + other, w + other);
  }

  vector4<T> operator-(const T &&other) {
    return vector4<T>(x - other, y - other, z - other, w - other);
  }

  vector4<T> operator*(const T &&other) {
    return vector4<T>(x*other, y*other, z*other, w*other);
  }

  vector4<T> operator/(const T &&other) {
    return vector4<T>(x/other, y/other, z/other, w/other);
  }

  vector4<T> operator%(const T &&other) {
    return vector4<T>(x%other, y%other, z%other, w%other);
  }

  bool operator==(const T &other) {
    return x==other && y==other && z==other && w==other;
  }

  bool operator!=(const T &other) {
    return x!=other || y!=other || z!=other || w!=other;
  }
};

typedef vector4<unsigned int> vector4u;
typedef vector4<int> vector4i;
typedef vector4<long> vector4l;
typedef vector4<float> vector4f;
typedef vector4<double> vector4d;
}

#endif //RA_ENGINE_VECTOR3_H
