//
// Created by main on 8/8/19.
//

#ifndef RA_ENGINE_SOURCE_RA_SYSTEM_INCLUDE_RA_SYSTEM_REGISTRY_HPP
#define RA_ENGINE_SOURCE_RA_SYSTEM_INCLUDE_RA_SYSTEM_REGISTRY_HPP

#include <memory>
#include <vector>

#include "ra/system/system.hpp"

namespace ra::system {
template <typename T>
class system_mod;

class registry {
public:
  template <typename> friend class system_mod;

  template <typename T, typename ...Args>
  system_mod<T> add_system(Args... args) {
    m_systems.push_back(std::make_unique<T>(args...));
  }

  void run_all() {

  }
protected:
  std::vector<std::unique_ptr<system>> m_systems;
};

template <typename T>
class system_mod {
public:
  system_mod(const std::size_t& p_pos, registry& p_registry) : m_pos(p_pos), m_registry(p_registry) {}

  void priority(decltype(system::priority) p_priority) {
    m_registry.m_systems.at(m_pos)->priority = p_priority;
  }

  registry& build() {
    std::sort(m_registry.m_systems.begin(), m_registry.m_systems.end(), [](std::unique_ptr<system> p_a, std::unique_ptr<system> p_b) {
      return p_a->priority < p_b->priority;
    });

    return m_registry;
  }
private:
  std::size_t m_pos;
  registry& m_registry;
};
}

#endif //RA_ENGINE_SOURCE_RA_SYSTEM_INCLUDE_RA_SYSTEM_REGISTRY_HPP
