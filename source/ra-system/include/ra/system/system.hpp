//
// Created by main on 8/7/19.
//

#ifndef RA_ENGINE_SOURCE_RA_SCHEDULING_INCLUDE_RA_SCHEDULING_SYSTEM_HPP
#define RA_ENGINE_SOURCE_RA_SCHEDULING_INCLUDE_RA_SCHEDULING_SYSTEM_HPP

#include <memory>

#include <ra/engine/engine_api.hpp>

namespace ra::engine { class engine; }

namespace ra::system {
class system {
public:
  virtual ~system() = default;

  virtual void run(const std::shared_ptr<ra::engine::engine_api>& p_engine, float delta) = 0;

  virtual bool request_speedup() = 0;
  virtual bool allow_slowdown() = 0;

  [[nodiscard]] virtual double rough_speed() const = 0;

  unsigned int priority;
  double current_exec_time;
  double last_exec_time;
  double target_exec_time;
protected:
private:
};
}

#endif //RA_ENGINE_SOURCE_RA_SCHEDULING_INCLUDE_RA_SCHEDULING_SYSTEM_HPP
