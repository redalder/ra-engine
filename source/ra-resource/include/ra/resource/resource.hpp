//
// Created by main on 5/23/19.
//

#ifndef RA_ENGINE_RESOURCE_HPP
#define RA_ENGINE_RESOURCE_HPP

#include <string>
#include <unistd.h>
#include <codecvt>

#include <platform_folders.h>

namespace ra::resource {
inline std::string get_config_home() {
  return sago::getConfigHome();
}

inline std::string get_data_home() {
  return sago::getDataHome();
}

inline std::string get_cache_dir() {
  return sago::getCacheDir();
}

inline std::string get_documents_folder() {
  return sago::getDocumentsFolder();
}

inline std::string get_desktop_folder() {
  return sago::getDesktopFolder();
}

inline std::string get_pictures_folder() {
  return sago::getPicturesFolder();
}

inline std::string get_music_folder() {
  return sago::getMusicFolder();
}

inline std::string get_video_folder() {
  return sago::getVideoFolder();
}

inline std::string get_download() {
  return sago::getDownloadFolder();
}

inline std::string get_save_game_folder1() {
  return sago::getSaveGamesFolder1();
}

inline std::string get_save_game_folder2() {
  return sago::getSaveGamesFolder2();
}

inline std::string get_asset_dir() {
#if BUILD_TYPE_DEBUG
#if __linux__
  const int bufsize = 512;
  char* buf = static_cast<char*>(malloc(512));
  readlink("/proc/self/exe", buf, bufsize);

  std::string exe_location = std::string(buf);

  exe_location = exe_location.substr(0, exe_location.find("/ra-engine/"))+"/ra-engine/assets/";
  free(buf);
  return exe_location;
#elif _WIN32
  HMODULE hModule = GetModuleHandleW(nullptr);
  WCHAR path[MAX_PATH];
  GetModuleFileNameW(hModule, path, MAX_PATH);

  std::wstring wide_exe_location = std::wstring(path);
  std::string exe_location = std::string(wide_exe_location.begin(), wide_exe_location.end());


  exe_location = exe_location.substr(0, exe_location.find("\\ra-engine\\"))  + "/ra-engine/assets/";
  return exe_location;
#endif
#elif BUILD_TYPE_RELEASE
#if __linux__
  const int bufsize = 512;
  char *buf = static_cast<char *>(malloc(512));
  readlink("/proc/self/exe", buf, bufsize);

  std::string exe_location = std::string(buf);

  exe_location = exe_location.substr(0, exe_location.find_last_of("/")) + "/assets/";
  free(buf);
  return exe_location;
#elif _WIN32
  HMODULE hModule = GetModuleHandleW(nullptr);
  WCHAR path[MAX_PATH];
  GetModuleFileNameW(hModule, path, MAX_PATH);

  std::wstring wide_exe_location = std::wstring(path);
  std::string exe_location = std::string(wide_exe_location.begin(), wide_exe_location.end());

  exe_location = exe_location.substr(0, exe_location.find_last_of("\\"))+"/assets/";
  return exe_location;
#endif
#endif
}
}

#endif //RA_ENGINE_RESOURCE_HPP
