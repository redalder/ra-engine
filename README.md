# RA-Engine

**Warning!** Compilation is only supported on Linux!

## Building

### Mingw
You'll need the x86_64-w64-mingw32 toolchain, most likely found in your distribution's repository.
Compile-time dependencies can all be installed by using `scripts/install-deps.bash` script. 
If the path to the x86_64-w64-mingw32 toolchain is different on your distribution, you can edit the script and change it.

### Clang/GCC
You'll need a fairly recent compiler, clang and gcc both work.
Needed dependencies are glfw3 and opengl development libraries. You can again get those from your distribution's repository.

## Running

### Wine
You'll need to copy some libraries into your wine prefix. The script `scripts/setup-winne-prefix.bash` does that for you.
You can change the location of your wine prefix by setting the enviroment variable `WINEPREFIX` to the absolute path of the wine prefix.
You also need a functional, OpenGL capable GPU.

### Linux
You only need glfw3 installed, doesn't have to be development version, and a functional OpenGL capable GPU.